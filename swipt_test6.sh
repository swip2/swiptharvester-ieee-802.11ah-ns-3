#!/bin/zsh

runs=10     #Parameter indicating how many times the simulation should be repeated.
seed=3      #Value of the radom seed
time=4800   #The duration of each simulation.
files=0     #Whether additional outputs should be displayed as .csv files

for ((run=1; run<=runs; run++))
do
    cd /home/user/repos/swiptharvester-ieee-802.11ah-ns-3

    for ((ps=0.1, i=1; ps<=0.9; ps=ps+0.1, i++ ))
    do    
        ./waf --run test4 --command="%s --seed=${seed} --RngRun=${run} --psFactor=${ps} --simulationTime=${time} --filesOutput=${files}" 
        cd ./ResultColetorSwipt/subdir
        for file in *.csv
        do
            mv "$file" ../results/"${file%.csv}"_"$i"_"$run".csv
        done      
        cd /home/user/repos/swiptharvester-ieee-802.11ah-ns-3
    done
done
