#!/usr/bin/python3
import os
import sys
import argparse
import subprocess
import threading
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from tokenize import Double
from subprocess import call
from array import *

#print("PATH Environment variable:", os.getenv("PATH"))
print("Current path",os.getcwd())
print("Running the Swipt Simulation for sensors distant from 5m up to 200m:")

# Distances from 5m to 200m in steps of 5m
distances = np.arange(5, 200, 5)
#distances = np.arange(5, 15, 5)
dist_size = distances.size

# For each 5m step in distance test the harvester with psFactor from 0 to 1
#psFactor = np.arange(0, 1.1, 0.1)
psFactor = "0.5"

# Number of Sensors
Nsta = 4

stas = np.arange(0, Nsta, 1)

idle_power = 20e-3
tx_power = 204e-3
rx_power = 92e-3
cca_busy_power = 92e-3
sleep_power = 99e-9

total_df = pd.DataFrame()
final_df = pd.DataFrame()

total_df_distance = []
total_df_station = []
total_df_idle_duration = []
total_df_rx_duration = []
total_df_tx_duration = []
total_df_cca_busy_duration = []
total_df_sleep_duration = []
total_df_totalEnergyHarvested = []
total_df_remaining_energy = []


for i in distances:
    radius = str(i)
    # Calling out the simulator
    output = subprocess.Popen(['./waf --run "udpdisk --radius=' + radius + ' --psFactor=' + psFactor + '"'], shell=True, stderr=subprocess.PIPE).communicate()[0]    
    #out = output.stderr.read(1)
    #sys.stdout.flush() 
    for j in stas:
        sta = str(j)
        df = pd.read_csv("./scratch/udpdisk/subdir/state_" + sta + ".csv", index_col = None)
        column = ["time", "state", "duration"]
        df.columns = column

        total_df_distance.append(i)
        total_df_station.append(j)
        
        idle = df[df['state'] == ' IDLE']
        total_df_idle_duration.append(idle["duration"].sum())

        rx = df[df["state"] == " RX"]
        total_df_rx_duration.append(rx["duration"].sum())

        tx = df[df["state"] == " TX"]
        total_df_tx_duration.append(tx["duration"].sum())

        cca_busy = df[df["state"] == " CCA_BUSY"]
        total_df_cca_busy_duration.append(cca_busy["duration"].sum())

        sleep = df[df["state"] == " SLEEP"]
        total_df_sleep_duration.append(sleep["duration"].sum())

    for k in stas:
        harvest = str(k)
        eh = pd.read_csv("./scratch/udpdisk/subdir/total_energy_harvested_"+ harvest +".csv", index_col=None)
        total_df_totalEnergyHarvested.append(eh.iloc[-1,2])

    for k in stas:
        remain = str(k)
        eh = pd.read_csv("./scratch/udpdisk/subdir/remaining_energy_"+ remain +".csv", index_col=None)
        total_df_remaining_energy.append(eh.iloc[-1,2])

total_df["distance"] = pd.Series(total_df_distance)
total_df["station"] = pd.Series(total_df_station)
total_df["idle_duration"] = pd.Series(total_df_idle_duration)
total_df["rx_duration"] = pd.Series(total_df_rx_duration)
total_df["tx_duration"] = pd.Series(total_df_tx_duration)
total_df["cca_busy_duration"] = pd.Series(total_df_cca_busy_duration)
total_df["sleep_duration"] = pd.Series(total_df_sleep_duration)
total_df["totalEnergyHarvested"] = pd.Series(total_df_totalEnergyHarvested)   
total_df["total_df_remaining_energy"] = pd.Series(total_df_remaining_energy)

# Calculate the Cosumed Energy by Each Station at each distance

total_df["idle_energy"] = total_df["idle_duration"]*idle_power
total_df["rx_energy"] = total_df["rx_duration"]*rx_power
total_df["tx_energy"] = total_df["tx_duration"]*tx_power
total_df["cca_busy_energy"] = total_df["cca_busy_duration"]*cca_busy_power
total_df["sleep_energy"] = total_df["sleep_duration"]*sleep_power
total_df["total_consump"] = total_df["idle_energy"] + total_df["rx_energy"] + total_df["tx_energy"] + total_df["cca_busy_energy"] + total_df["sleep_energy"]

sta0 = total_df[total_df['station']==0]
sta1 = total_df[total_df['station']==1]
sta2 = total_df[total_df['station']==2]
sta3 = total_df[total_df['station']==3]

fig, ax = plt.subplots()
ax.plot(sta0["distance"], sta0["total_consump"], color="blue", label="Total Consump")
ax.plot(sta0["distance"], sta0["totalEnergyHarvested"], color="red", label="Energy Harvested")
ax.plot(sta0["distance"], sta0["total_df_remaining_energy"], color="orange", label="Remaining Energy")
ax.legend()    
plt.title("Sensor 0")
plt.xlabel("Diantance (m)")
plt.ylabel("Energy Consumed(J) vs Energy Harvested (J)")
plt.show()

fig, bx = plt.subplots()     
bx.plot(sta1["distance"], sta1["total_consump"], color="blue", label="Total Consump")
bx.plot(sta1["distance"], sta1["totalEnergyHarvested"], color="red", label="Energy Harvested")
bx.plot(sta1["distance"], sta1["total_df_remaining_energy"], color="orange", label="Remaining Energy")
bx.legend()    
plt.title("Sensor 1")
plt.xlabel("Diantance (m)")
plt.ylabel("Energy Consumed(J) vs Energy Harvested (J)")
plt.show()

fig, cx = plt.subplots()     
cx.plot(sta2["distance"], sta2["total_consump"], color="blue", label="Total Consump")
cx.plot(sta2["distance"], sta2["totalEnergyHarvested"], color="red", label="Energy Harvested")
cx.plot(sta2["distance"], sta2["total_df_remaining_energy"], color="orange", label="Remaining Energy")
cx.legend()    
plt.title("Sensor 2")
plt.xlabel("Diantance (m)")
plt.ylabel("Energy Consumed(J) vs Energy Harvested (J)")
plt.show()

fig, dx = plt.subplots()     
dx.plot(sta3["distance"], sta3["total_consump"], color="blue", label="Total Consump")
dx.plot(sta3["distance"], sta3["totalEnergyHarvested"], color="red", label="Energy Harvested")
dx.plot(sta3["distance"], sta3["total_df_remaining_energy"], color="orange", label="Remaining Energy")
dx.legend()    
plt.title("Sensor 3")
plt.xlabel("Diantance (m)")
plt.ylabel("Energy Consumed(J) vs Energy Harvested (J)")
plt.show()


#os.system("ls -la")
#print("List files using the subprocess module:")
#call(["ls", "-la"])
#os.system(["pwd"])
