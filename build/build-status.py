#! /usr/bin/env python

# Programs that are runnable.
ns3_runnable_programs = ['build/scratch/test4/ns3-dev-test4-debug', 'build/scratch/ns3-dev-scratch-simulator-debug', 'build/scratch/ns3-dev-RAW-generate-debug']

# Scripts that are runnable.
ns3_runnable_scripts = []

