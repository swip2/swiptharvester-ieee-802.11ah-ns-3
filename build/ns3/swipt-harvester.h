/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2014 Wireless Communications and Networking Group (WCNG),
 * University of Rochester, Rochester, NY, USA.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Cristiano Tapparello <cristiano.tapparello@rochester.edu>
 */

#ifndef SWIPT_HARVESTER_H
#define SWIPT_HARVESTER_H

#include <iostream>

// include from ns-3
#include "energy-harvester.h"
#include "basic-energy-source.h"
#include "li-ion-energy-source.h"
#include "ns3/traced-value.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/nstime.h"
#include "ns3/event-id.h"
#include "ns3/random-variable-stream.h"
#include "ns3/device-energy-model.h"
#include "ns3/wifi-module.h"
#include "ns3/wifi-phy.h"
#include "ns3/yans-wifi-phy.h"
#include "ns3/object.h"
#include "ns3/log.h"
#include "ns3/assert.h"
#include "ns3/pointer.h"
#include "ns3/string.h"
#include "ns3/simulator.h"
#include "ns3/uinteger.h"
#include "ns3/radiotap-header.h"
#include "ns3/packet.h"
#include "ns3/interference-helper.h"
#include "ns3/callback.h"


#include <iomanip>
#include <stdint.h>
#include <vector>
#include <list>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <cmath>

namespace ns3 {

class WifiPhy;
class WifiPhyListener;
class WifiNetDevice;
class YansWifiChannel;

/**
 * \ingroup energy
 * SwiptHarvester increases remaining energy stored in an associated
 * Energy Source. The SwiptHarvester implements a simple model in which
 * the amount of power provided by the harvester varies according
 * to the power of the packets received.
 *
 * Unit of power is chosen as Watt since energy models typically calculate
 * energy as (time in seconds * power in Watt).
 *
 */
class SwiptHarvester: public EnergyHarvester
{
public:
  static TypeId GetTypeId (void);

  SwiptHarvester ();
  virtual ~SwiptHarvester ();

  void PowerWPktReceived (Ptr<const Packet> packet,
                          double rxPowerDbm,
                          WifiTxVector txVector,
                          enum WifiPreamble preamble,
                          uint8_t packetType, Time rxDuration, Ptr<YansWifiPhy> wifiPhyPtr);
  void PowerWPktReceivedDepletion (Ptr<const Packet> packet,
                                  double rxPowerDbm,
                                  WifiTxVector txVector,
                                  enum WifiPreamble preamble,
                                  uint8_t packetType, Time rxDuration, Ptr<YansWifiPhy> wifiPhyPtr);                        
  double GetRxPowerW (void);
  void NotifyRxStartNow (Time duration);
  void NotifyNotRx (void);
  void UpdateSwiptHarvester (void);
  void SetUpSwiptPhyListener (Ptr<WifiPhy> phy);
  void UnregisterSwiptPhyListener (Ptr<WifiPhy> phy);
  void SetAntennaNoise (double antennaNoise);
  double GetAntennaNoise (void) const;
  double DbmToW (double dBm) const;

  void SetChannel (Ptr<YansWifiChannel> channel);

  void NotifyPlcpHeader (double noiseInterferenceW, 
                        Time noiseInterferenceWDuration, 
                        Ptr<SwiptHarvester> swiptH);

  void NotifyPlcpPayload (double noiseInterferenceW, 
                        Time noiseInterferenceWDuration, 
                        Ptr<SwiptHarvester> swiptH);

  double m_noiseInterferenceW;

  void CaptureEnergyAllPkts (double psFactor);
  void RechargedBattery (void);

  /**
   * \param psFactor ratio of rxPowerW divided between the receiver and the harvester.
   *
   * This function takes on the parameter is used to calculate the amount of power
   * to be used by the SwiptHarvester to recharge the energy source.
   */
  void SetPowerSplitFactor (double psFactor);

  /**
   * \returns The ratio of rxPowerW divided between the receiver and the harvester.
   *
   * This function returns the ratio of power divided between the receiver and the
   * SwiptHarvester.
   */
  double GetPowerSplitFactor (void) const;

    /**
   * \param eta AC-DC conversion eficiency from the SwiptHarvester.
   *
   * This function sets the parameter used to calculate the efficiency of the
   * harvester according to 2021_Lou_On the Study of Sustainability and Outage of SWIPT-Enabled Wireless Communications.
   */
  void SetSwiptEfficiency (double eta);

  /**
   * \returns AC-DC conversion eficiency from the SwiptHarvester.
   *
   * This function returns on the parameter is used to calculate the efficiency of the
   * harvester according to 2021_Lou_On the Study of Sustainability and Outage of SWIPT-Enabled Wireless Communications.
   */
  double GetSwiptEfficiency (void) const;

    /**
   * \param beta DC-DC energy conversion eficiency from the harvester to the battery.
   *
   * This function sets the parameter used to calculate the efficiency of the
   * harvester according to 2021_Lou_On the Study of Sustainability and Outage of SWIPT-Enabled Wireless Communications.
   */
  void SetDCConversionEfficiency (double beta);

  /**
   * \returns DC-DC energy conversion eficiency from the harvester to the battery.
   *
   * This function returns on the parameter is used to calculate the efficiency of the
   * harvester according to 2021_Lou_On the Study of Sustainability and Outage of SWIPT-Enabled Wireless Communications.
   */
  double GetDCConversionEfficiency (void) const;

  void NotifyTxStartNow (Time duration, double txPowerDbm);
  void NotifyRxEndOkNow (void);
  void NotifyRxEndErrorNow (void);
  void NotifyMaybeCcaBusyStartNow (Time duration);
  void NotifySwitchingStartNow (Time duration);
  void NotifySleepNow (void);
  void NotifyWakeupNow (void);
  void NotifyOffNow (void);
  void NotifyOnNow (void);
  void NotifyIdleNow (void);
  void Calculate (void);

  void SetDepletionMode (double interPacketIntervalDepletion);
  void ExitDepletionMode (void);
  double GetTotalEnergyDepletionMode (void);
  bool GetDepletionMode (void);
  void UpdateEnergySourceDepletion (double m_energyAllPkts);
  void UpdateEnergySourceSleep (Ptr<const Packet> packet,
                                        WifiTxVector txVector,
                                        enum WifiPreamble preamble,
                                        uint8_t packetType, 
                                        Time rxPktDuration, 
                                        Ptr<YansWifiPhy> phyObject,
                                        double rxPowerSleep,
                                        double energySleep);

  //Ptr<SwiptInterference::Event> m_event;

  double m_plcpHeaderPowerHarvested;
  double m_plcpHeaderEnergyHarvested;

  double m_plcpPayloadPowerHarvested;
  double m_plcpPayloadEnergyHarvested;

  //void SwitchToIdle (void);
  //void NotifyHarvestedPower (double power);
  //void NotifyHarvestedEnergy (double energy);

  bool m_off;
  bool m_sleep;
  bool m_maybeCca;
  bool m_rxing2;
  bool m_txing;
  bool m_switching;
  bool m_idle;

private:
  /// Defined in ns3::Object
  void DoInitialize (void);

  /// Defined in ns3::Object
  void DoDispose (void);

  /**
   * \returns m_harvestedPower The power currently provided by the Basic Energy Harvester.
   * Implements DoGetPower defined in EnergyHarvester.
   */
  virtual double DoGetPower (void) const;
  //virtual double DoGetPower (void);

  /**
   * This function is called every m_energyHarvestingUpdateInterval in order to
   * update the amount of power that will be provided by the harvester in the
   * next interval.
   */
  void UpdateHarvestedPower (void);
  void UpdateSwiptHarvesterDepletion (double rxPow, Time duration);

  void OutputData (void);

  //typedef void (* TracedCallback)(const Ptr< SwiptHarvester> swipt);


  //typedef void (* SwiptPowerTracedCallback)
  //  (const std::string context, Ptr<const SwiptHarvester> swipt);  
    
//  typedef void (* SwiptTotalEnergyTracedCallback)
//    (const std::string context, Ptr<const SwiptHarvester> swipt);
  
private:

  bool m_registered;
  bool m_unregistered;
  bool m_depletionMode;
  
  double m_eta;
  double m_beta;
  double m_rxPowerW;
  double m_rxPowerDbm;
  double m_psFactor;
  double m_PreviousPsFactor;
  double m_antennaNoise;
  double m_interferenceWHeader;
  double m_interferenceWPayload;

  //double m_swiptHarvestedPower;         // current harvested power, in Watt
  //double m_swipTotalEnergyHarvestedJ;  // total harvested energy, in Joule

  Ptr<WifiPhy> m_wifiPhy;
  //Ptr<WifiNetDevice> m_wifiNetDevice;
  
  Time m_duration;
  Time m_rxDuration;
  Time m_updateInterval;
  Time m_eventDurationHeader;
  Time m_eventDurationPayload;
  Time m_headerDuration;

  TracedValue<double> m_totalHarvestedPower;         // current harvested power, in Watt
  TracedValue<double> m_totalEnergyHarvestedJ;  // total harvested energy, in Joule

  //TracedValue<double> m_totalHarvestedPowerAllPkts;         // current harvested power, in Watt
  //TracedValue<double> m_totalEnergyHarvestedJAllPkts;  // total harvested energy, in Joule

  class SwiptPhyListener * m_swiptPhyListener;

  //ns3::TracedCallback<Ptr<SwiptHarvester> > m_harvestedPower;
  //ns3::TracedCallback<Ptr<SwiptHarvester> > m_totalEnergyHarvestedJ;

  EventId m_energyUpdateEvent;// energy update event
  EventId m_energyUpdateDepletionEvent;// energy update event

  double noiseInterferenceHeaderW;
  double noiseInterferencePayloadW;

  double noiseInterferenceHeaderJ;
  double noiseInterferencePayloadJ;

  double m_harvestedPowerAllPkts;
  double m_energyHarvestedAllPkts;
  double m_previousTotalEnergyHarvestedJAllPkts;

  Time m_interfHeader;
  Time m_interfPayload;

  double m_energyHarvested;
  double m_harvestedPower;

  double totalInterferenceHeader = 0;
  double totalInterferencePayload = 0;

  double m_notifySource;
  Ptr<YansWifiPhy> phyLayer;
};


} // namespace ns3

#endif /* defined(BASIC_ENERGY_HARVESTER) */
