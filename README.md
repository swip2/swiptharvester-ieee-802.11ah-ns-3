### SWIPT Harvester IEEE-802.11ah-ns-3


This repository hosts the implementation of the Simultaneous Wireless Information and Power Transfer (SWIPT) model over the IEEE802.11ah (Wi-Fi HaLow) protocol for the NS-3 network simulator. Our work is described in the paper accepted to the Workshop on ns-3 (WNS3 2024):

> José Antônio de França Junior and Marcelo M. Carvalho. 2024. **An ns-3
Model for Simultaneous Wireless Information and Power Transfer (SWIPT)
over IEEE 802.11ah Networks.** In 2024 Workshop on ns-3 (WNS3 2024), June 5–6, 2024, Barcelona, Spain. ACM, New York, NY, USA, 9 pages. https://doi.org/10.1145/3659111.3659113. 

Our implementation was built upon the IEEE 802.11ah NS-3 Simulation Module, as outlined in:

> Le Tian, Amina Sljivo, Serena Santi, Eli De Poorter, Jeroen Hoebeke, Jeroen Famaey. **Extension of the IEEE 802.11ah NS-3 Simulation Module.** Workshop on ns-3 (WNS3), 2018. Available at [ResearchGate](https://www.researchgate.net/publication/324910418_Extension_of_the_IEEE_80211ah_ns-3_Simulation_Module).


### Installation and usage instructions ###
* The recommended operating system for installing this implementation is Ubuntu 20.04.6 LTS.
* Clone the project from git
* Follow the instructions on https://www.nsnam.org/wiki/Installation to prepare all dependencies. For Ubuntu 20.04.6 LTS, those are:
```
sudo apt-get update
sudo apt-get -y install gcc g++ python
sudo apt-get -y install gcc g++ python python-dev
sudo apt-get -y install qt4-dev-tools libqt4-dev
sudo apt-get -y install mercurial
sudo apt-get -y install bzr
sudo apt-get -y install cmake libc6-dev libc6-dev-i386 g++-multilib
sudo apt-get -y install gdb valgrind
sudo apt-get -y install gsl-bin libgsl2 libgsl2:i386
sudo apt-get -y install flex bison libfl-dev
sudo apt-get -y install tcpdump
sudo apt-get -y install sqlite sqlite3 libsqlite3-dev
sudo apt-get -y install libxml2 libxml2-dev
sudo apt-get -y install libgtk2.0-0 libgtk2.0-dev
sudo apt-get -y install vtun lxc
sudo apt -y install git
git clone https://gitlab.com/swip2/swiptharvester-ieee-802.11ah-ns-3
```
* Change into the swiptharvester-ieee-802.11ah-ns-3 directory.  
* Configure waf:
`CXXFLAGS="-std=c++11" ./waf configure --disable-examples --disable-tests`
* Build:
`./waf`
* Run the simulation:
`./waf --run test4`

### Network Topology ###

Our code was specifically designed to function with the following network topology, which consists of 1 AP nd 30 nodes.

![Network topology](nt_topology.png)

The Nodes were divided into 3 RAW Groups with 10 nodes each:


* The 1st group consists of nodes with NodeId from 0 to 9, positioned at a radial distance of 1m from the AP. 

* The 2nd group consists of nodes from 10 to 19, positioned at a radial distance of 1.75m from the AP.

* The 3rd group consists of nodes from 20 to 29, positioned at a radial distance of 2.5m.


### Simulation Parameters ###

The configuration of all parameters used in the project, as well as for running our simulations, are presented in the table below.

| Parameter                  | Node     | Access Point |
| ------                     | ------   |  ------      |
|Radio Frequency             | 900 MHz  |  900 MHz     |
|Beacon interval             | ------   |  102400 𝜇s   |
|Beacon Frame Duration       | ------   |  2800 𝜇s     |
|Transmission Power          | 0 dBm    |  30 dBm      |
|Antenna Gain                | 6 dBi    |  13 dBi      |
|Short Guard Enabled         | False    |  False      |
|Channel Width               | 1 MHz    |  1 MHz      |
|Energy Detection Threshold  | -110 dBm |  -110 dBm      |
|CCA Mode1 Threshold         | -107 dBm |  -107 dBm      |
|LDPC Enabled                | False    |  False     |       
|S1G Short Field Enabled     | False    |  False      |
|S1G 1M Field Enabled        | True     |  True      |
|Noise Figure                | 6.8      |  6.8      |
|Parameters 𝛽 , 𝜂            | 0.9 , 0.95 |  ------    |
|Data Mode                   | 600 kbps |  600 kbps    |
|Control Mode                | 300 kbps |  300 kbps      |
|PLCP Preamble TX Rate       | 150 kbps |  150 kbps      |
|PLCP Header TX Rate         | 150 kbps |  150 kbps      |
|UDP payload size            | 64 bytes |  ------      |
|UDP packet interval         | 3.072s   |  ------      |


For further details, please refer to the accompanying paper:

> José Antônio de França Junior and Marcelo M. Carvalho. 2024. **An ns-3
Model for Simultaneous Wireless Information and Power Transfer (SWIPT)
over IEEE 802.11ah Networks.** In 2024 Workshop on ns-3 (WNS3 2024), June 5–6, 2024, Barcelona, Spain. ACM, New York, NY, USA, 9 pages. https://doi.org/10.1145/3659111.3659113. 

### Simulation Script: ###

The shell script `swipt_test6.sh` was utilized to execute our simulations. It is located within the swiptharvester-ieee-802.11ah-ns-3 directory.

* Change into swiptharvester-ieee-802.11ah-ns-3 directory.  

* Check the contents of the shell script `swipt_test6.sh`:

    `cat ./swipt_test6.sh`

* You should see the following output:

```
1       #!/bin/zsh
2 
3       runs=10     #The number of times the simulation is repeated
4       seed=3      #Value of the random seed
5       time=4800   #The lengh of time the each simulation, in seconds
6       files=0     #Whether additional output should be displayed as .csv files. To turn off additional output: files=0, to turn on: files=1
7 
8       for ((run=1; run<=runs; run++))
9       do
10          cd /home/user/repos/swiptharvester-ieee-802.11ah-ns-3
11
12          for ((ps=0.1, i=1; ps<=0.9; ps=ps+0.1, i++ ))
13          do    
14              ./waf --run test4 --command="%s --seed=${seed} --RngRun=${run} --psFactor=${ps} --simulationTime=${time} --filesOutput=${files}" 
15              cd ./ResultColetorSwipt/subdir
16              for file in *.csv
17              do
18                  mv "$file" ../results/"${file%.csv}"_"$i"_"$run".csv
19              done      
20              cd /home/user/repos/swiptharvester-ieee-802.11ah-ns-3
21          done
22      done
```
* Modify the path to the swiptharvester-ieee-802.11ah-ns-3 directory on lines 10 and 20 of the shell script using your preferred text editor to match your installation setup.

    `/home/user/repos/swiptharvester-ieee-802.11ah-ns-3`

* Additional parameters, including the simulation repetition count, random seed value, simulation duration, and output display options, can be configured by adjusting the following parameters:

```
runs=10     #The number of times the simulation is repeated
seed=3      #Value of the random seed
time=4800   #The lengh of time the each simulation, in seconds
files=0     #Whether additional output should be displayed as .csv files. To turn off additional output: files=0, to turn on: files=1
```
* Clear any files that may be present in the output directory: /swiptharvester-ieee-802.11ah-ns-3/ResultCollectorSwipt, by executing the './clean.sh' shell script before executing the simulation shell script.

    * Change into ResultCollectorSwipt subdirectory.

    * Execute the './clean.sh' shell script.

        `./clean.sh`

    * Change back to the swiptharvester-ieee-802.11ah-ns-3 directory.


* Execute the simulation shell script.

    `./swipt_test6.sh`

### Results Subdirectory ###

The results of the simulations obtained by running `swipt_test6.sh` shell script can be found at the /swiptharvester-ieee-802.11ah-ns-3/ResultColetorSwipt/results subdirectoty.

To generate the graphs presented in the paper, you can utilize the 'network_mean3v10.ipynb' Jupyter Notebook, available within the /swiptharvester-ieee-802.11ah-ns-3/ResultColetorSwipt/ directory.

Note:

* We configured the 'network_mean3v10.ipynb' notebook using the Jupyter Extension for Visual Studio Code.

* The Python version used is 3.8.10.

* Ensure the following Python packages are installed :


```
cmath, subprocess, tokenize, matplotlib.pyplot, matplotlib.ticker, numpy, pandas, seaborn, ipydatagrid, IPython.display, ipywidgets , mpl_toolkits , mpl_toolkits.mplot3d , collections , cycler, math, statistics and scipy.stats.
```


### Other parameters ###

Other parameters of the IEEE802.11ah (Wi-Fi HaLow) protocol for the NS-3 network simulator presented at https://github.com/imec-idlab/IEEE-802.11ah-ns-3 have not been thoroughly tested in our project. Therefore, we cannot guarantee the expected results for them.