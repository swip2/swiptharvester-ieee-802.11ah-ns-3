/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2014 Wireless Communications and Networking Group (WCNG),
 * University of Rochester, Rochester, NY, USA.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * adouble with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Cristiano Tapparello <cristiano.tapparello@rochester.edu>
 */

#include "swipt-harvester.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("SwiptHarvester");

NS_OBJECT_ENSURE_REGISTERED (SwiptHarvester);

TypeId
SwiptHarvester::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::SwiptHarvester")
  .SetParent<EnergyHarvester> ()
  .SetGroupName ("Energy")
  .AddConstructor<SwiptHarvester> ()
  .AddAttribute ("PowerSplitFactor",
                  "The ratio of rxPowerW shared between the receiver and the harvester",
                  DoubleValue (0.5),
                  MakeDoubleAccessor (&SwiptHarvester::SetPowerSplitFactor,
                                      &SwiptHarvester::GetPowerSplitFactor),
                  MakeDoubleChecker<double> ())   
  .AddAttribute ("AntennaNoise",
                  "RX antenna noise in dBm", 
                  DoubleValue (-111.0), // -111dBm from the pcap files generated during simulation
                  MakeDoubleAccessor (&SwiptHarvester::SetAntennaNoise,
                                      &SwiptHarvester::GetAntennaNoise),
                  MakeDoubleChecker<double> ())  
  .AddAttribute ("SwiptEfficiency",
                 "AC-DC conversion eficiency from the SwiptHarvester",
                 DoubleValue (0.9),
                 MakeDoubleAccessor (&SwiptHarvester::SetSwiptEfficiency,
                                     &SwiptHarvester::GetSwiptEfficiency),
                 MakeDoubleChecker<double> ())  
  .AddAttribute ("DCConversionEfficiency",
                 "DC-DC energy conversion eficiency from the harvester to the battery",
                 DoubleValue (0.95),
                 MakeDoubleAccessor (&SwiptHarvester::SetDCConversionEfficiency,
                                     &SwiptHarvester::GetDCConversionEfficiency),
                 MakeDoubleChecker<double> ()) 
  //.AddAttribute ("FilenameVoltageTracking",
  //                "Name of the output file where to save voltage values", StringValue (),
  //                MakeStringAccessor (&SwiptHarvester::m_filenameVoltageTracking),
  //                MakeStringChecker ())
  .AddTraceSource ("HarvestedPower",
                   "Harvested power by the SwiptHarvester.",
                   MakeTraceSourceAccessor (&SwiptHarvester::m_totalHarvestedPower),
                   "ns3::TracedValue::DoubleCallback")
  .AddTraceSource ("TotalEnergyHarvested",
                   "Total energy harvested by the SwiptHarvester.",
                   MakeTraceSourceAccessor (&SwiptHarvester::m_totalEnergyHarvestedJ),
                   "ns3::TracedValue::DoubleCallback")
  ;
  return tid;
}

class SwiptPhyListener : public WifiPhyListener
{
public:
  /**
   * Create a SwiptPhyListener for a given Harvester.
   *
   */
    SwiptPhyListener (ns3::SwiptHarvester *swiptHarvester)
    : m_swiptHarvester (swiptHarvester)
    {
        NS_LOG_FUNCTION(this);
    }
    virtual ~SwiptPhyListener ()
    {
        NS_LOG_FUNCTION(this);
    }

    virtual void NotifyRxStart (Time duration)
    {
      NS_LOG_FUNCTION(this << duration);
      m_swiptHarvester->NotifyRxStartNow (duration);
      //NS_LOG_FUNCTION ( Simulator::Now().GetSeconds() << "ns, Node:"<< GetNode ()->GetId () << ", SwiptPhyListener NotifyRxStartNow: " << duration);
    }

    virtual void NotifyRxEndOk (void)
    {
      NS_LOG_FUNCTION(this);
      m_swiptHarvester->NotifyRxEndOkNow();
      //std::cout << Simulator::Now().GetSeconds() << "ns, SwiptPhyListener NotifyRxEndOk " << std::endl;
    }

    virtual void NotifyRxEndError (void)
    {
      NS_LOG_FUNCTION(this);
      m_swiptHarvester->NotifyRxEndErrorNow();
      //std::cout << Simulator::Now().GetSeconds() << "ns, SwiptPhyListener NotifyRxEndError " << std::endl;
    }
    
    virtual void NotifyTxStart (Time duration, double txPowerDbm)
    {
      NS_LOG_FUNCTION(this << duration << txPowerDbm);
      m_swiptHarvester->NotifyTxStartNow (duration, txPowerDbm);
      //std::cout << Simulator::Now().GetSeconds() << "ns, SwiptPhyListener NotifyTxStart, duration: " << duration << " ,txPowerDbm: " << txPowerDbm << std::endl;
    }

    virtual void NotifyMaybeCcaBusyStart (Time duration)
    {
      NS_LOG_FUNCTION(this << duration);
      m_swiptHarvester->NotifyMaybeCcaBusyStartNow(duration);
      //std::cout << Simulator::Now().GetSeconds() << "ns, SwiptPhyListener NotifyMaybeCcaBusyStart " << std::endl;
    }

    virtual void NotifySwitchingStart (Time duration)
    {
      NS_LOG_FUNCTION(this << duration);
      m_swiptHarvester->NotifySwitchingStartNow(duration);
      //std::cout << Simulator::Now().GetSeconds() << "ns, SwiptPhyListener NotifySwitchingStart " << std::endl;
    }

    virtual void NotifySleep (void)
    {
      NS_LOG_FUNCTION(this);
      m_swiptHarvester->NotifySleepNow ();
      //std::cout << Simulator::Now().GetSeconds() << "ns, SwiptPhyListener NotifySleep " << std::endl;
    }

    virtual void NotifyWakeup (void)
    {
      NS_LOG_FUNCTION(this);
      m_swiptHarvester->NotifyWakeupNow();
      //std::cout << Simulator::Now().GetSeconds() << "ns, SwiptPhyListener NotifyWakeup " << std::endl;
    }
        
    virtual void NotifyOff (void)
    {
      NS_LOG_FUNCTION(this);
      m_swiptHarvester->NotifyOffNow ();
      //std::cout << Simulator::Now().GetSeconds() << "ns, SwiptPhyListener NotifyOff " << std::endl;
    }     

    virtual void NotifyOn (void)
    {
      NS_LOG_FUNCTION(this);
      m_swiptHarvester->NotifyOnNow ();
      //std::cout << Simulator::Now().GetSeconds() << "ns, SwiptPhyListener NotifyOn " << std::endl;
    }

    virtual void NotifyIdle (void)
    {
      NS_LOG_FUNCTION(this);
      m_swiptHarvester->NotifyIdleNow ();
      //std::cout << Simulator::Now().GetSeconds() << "ns, SwiptPhyListener NotifyOn " << std::endl;
    }

private:
    ns3::SwiptHarvester *m_swiptHarvester;
};

SwiptHarvester::SwiptHarvester ()
{
  NS_LOG_FUNCTION (this);
  m_rxPowerW = 0.0;
  m_energyHarvested = 0.0;
  m_totalEnergyHarvestedJ = 0.0;
  m_totalHarvestedPower = 0.0;
  m_plcpHeaderPowerHarvested = 0.0;
  m_plcpPayloadPowerHarvested = 0.0;
  m_harvestedPower = 0.0;
  m_energyHarvested = 0.0;
}

SwiptHarvester::~SwiptHarvester ()
{
  NS_LOG_FUNCTION (this);
  m_rxPowerW = 0.0;
  m_energyHarvested = 0.0;
  m_totalEnergyHarvestedJ = 0.0;
  m_totalHarvestedPower = 0.0;
  m_plcpHeaderPowerHarvested = 0.0;
  m_plcpPayloadPowerHarvested = 0.0;
  m_harvestedPower = 0.0;
  m_energyHarvested = 0.0;
}

void 
SwiptHarvester::PowerWPktReceived (Ptr<const Packet> packet,
                                        double rxPowerDbm,
                                        WifiTxVector txVector,
                                        enum WifiPreamble preamble,
                                        uint8_t packetType, 
                                        Time rxPktDuration, 
                                        Ptr<YansWifiPhy> phyObject)
{

  Time now = Simulator::Now ();

  double rxPowerW;
  phyLayer = phyObject;

  WifiMacHeader hdr;
  packet->PeekHeader(hdr);

  if (phyObject->IsStateIdle () || phyObject->IsStateCcaBusy ())
  {
      NS_LOG_FUNCTION (this << Simulator::Now ().GetNanoSeconds() << phyObject);
      uint32_t nodeId = phyObject->GetDevice ()->GetObject<WifiNetDevice> ()-> GetNode () -> GetId ();
      double rxAntennaGain = m_wifiPhy->GetObject<YansWifiPhy> ()->GetRxGain ();
      rxPowerW = DbmToW (rxPowerDbm + rxAntennaGain) + DbmToW (m_antennaNoise);
      m_duration = rxPktDuration;

      if (m_psFactor > 0.95)
      {
        m_rxPowerW = 0.0;
        rxPowerW = 0.0;
        m_plcpHeaderPowerHarvested = 0.0;
        m_plcpPayloadPowerHarvested = 0.0;
        m_harvestedPower = 0.0;
        m_energyHarvested = 0.0;
      }
      else if (m_psFactor <= 0.95)
      {
        //Time now = Simulator::Now ();
        double inv_psFactor =  1.00 - m_psFactor;
        m_rxPowerW = rxPowerW * inv_psFactor;

      }
  } 
  else
  {
    m_rxPowerW = 0.0;
    rxPowerW = 0.0;
    m_plcpHeaderPowerHarvested = 0.0;
    m_plcpPayloadPowerHarvested = 0.0;
    m_harvestedPower = 0.0;
    m_energyHarvested = 0.0;
    //UpdateSwiptHarvester ();
  }

}   

void
SwiptHarvester::UpdateSwiptHarvester (void)
{
  NS_LOG_FUNCTION (this << "UpdateSwiptHarvester");
  m_energyHarvested = 0.0;
  m_harvestedPower = 0.0;

  // Do not update if simulation has stopped
  if (Simulator::IsFinished ())
  {
    NS_LOG_DEBUG ("SwiptHarvester: Simulation Finished.");
    return;
  }

  // Calculate the Power Harvested
  m_harvestedPower = m_rxPowerW * m_eta;

  // Calculate the Energy Harvested
  m_energyHarvested = m_harvestedPower * m_duration.GetSeconds ();


    // Update the total energy harvested
    m_totalEnergyHarvestedJ += m_energyHarvested;

    //Update the total energy harvested
    m_totalHarvestedPower += m_harvestedPower;

}



void
SwiptHarvester::UpdateEnergySourceSleep (Ptr<const Packet> packet,
                                        WifiTxVector txVector,
                                        enum WifiPreamble preamble,
                                        uint8_t packetType, 
                                        Time rxPktDuration, 
                                        Ptr<YansWifiPhy> phyObject,
                                        double rxPowerSleep,
                                        double energySleep)
{
  NS_LOG_FUNCTION (this << energySleep);

  uint32_t nodeId = phyObject->GetDevice ()->GetObject<WifiNetDevice> ()-> GetNode () -> GetId ();


  m_totalHarvestedPower += rxPowerSleep;
  m_totalEnergyHarvestedJ += energySleep;

  Ptr<LiIonEnergySource> bat =  GetEnergySource ()-> GetObject<LiIonEnergySource> ();
  bat->UpdateEnergySleep(energySleep);

}


double
SwiptHarvester::DbmToW (double dBm) const
{
  NS_LOG_FUNCTION(this << dBm);
  double mW = std::pow (10.0, dBm / 10.0);
  return mW / 1000.0;
}

double
SwiptHarvester::GetRxPowerW (void)
{
  NS_LOG_FUNCTION (this);
  return m_rxPowerW;
}

void
SwiptHarvester::SetAntennaNoise (double antennaNoise)
{
  NS_LOG_FUNCTION (this << antennaNoise);
  m_antennaNoise = antennaNoise;
}

double
SwiptHarvester::GetAntennaNoise (void) const
{
  NS_LOG_FUNCTION (this);
  return m_antennaNoise;
}

void
SwiptHarvester::SetUpSwiptPhyListener (Ptr<WifiPhy> phy)
{
  NS_LOG_FUNCTION (this << phy);
  m_rxing2 = false;
  m_swiptPhyListener = new SwiptPhyListener (this);
  m_wifiPhy = phy;
  m_wifiPhy->RegisterListener (m_swiptPhyListener);
  //std::cout << "\n ====\n SetUpSwiptPhyListener: " << m_swiptPhyListener << "\n====\n" << ", rxing="<< m_rxing2 << std::endl;
}

void
SwiptHarvester::UnregisterSwiptPhyListener (Ptr<WifiPhy> phy)
{
  NS_LOG_FUNCTION (this << phy);

  phy->UnregisterListener (m_swiptPhyListener);
  m_registered = false;
  m_unregistered = true;
}

void
SwiptHarvester::DoInitialize (void)
{
  NS_LOG_FUNCTION (this);
  m_rxPowerW = 0.0;
  m_energyHarvested = 0.0;
  m_totalEnergyHarvestedJ = 0.0;
  m_totalHarvestedPower = 0.0;
  m_plcpHeaderPowerHarvested = 0.0;
  m_plcpPayloadPowerHarvested = 0.0;
  m_harvestedPower = 0.0;
  m_energyHarvested = 0.0;

  //std::cout << Simulator::Now().GetNanoSeconds() << "ns ==== DoInitialize ====\n" << ", rxing="<< m_rxing2 << std::endl;
}

void
SwiptHarvester::DoDispose (void)
{
  NS_LOG_FUNCTION (this);
  m_rxPowerW = 0.0;
  m_energyHarvested = 0.0;
  m_totalEnergyHarvestedJ = 0.0;
  m_totalHarvestedPower = 0.0;
  m_plcpHeaderPowerHarvested = 0.0;
  m_plcpPayloadPowerHarvested = 0.0;
  m_harvestedPower = 0.0;
  m_energyHarvested = 0.0;
}

void
SwiptHarvester::SetPowerSplitFactor (double psFactor)
{
  NS_LOG_FUNCTION (this << psFactor);
  m_psFactor = psFactor;
}

double
SwiptHarvester::GetPowerSplitFactor (void) const
{
  NS_LOG_FUNCTION (this);
  return m_psFactor;
}

void
SwiptHarvester::SetSwiptEfficiency (double eta)
{
  NS_LOG_FUNCTION (this << eta);
  m_eta = eta;
}

double
SwiptHarvester::GetSwiptEfficiency (void) const
{
  NS_LOG_FUNCTION (this);
  return m_eta;
}

void
SwiptHarvester::SetDCConversionEfficiency (double beta)
{
  NS_LOG_FUNCTION (this << beta);
  m_beta = beta;
}

double
SwiptHarvester::GetDCConversionEfficiency (void) const
{
  NS_LOG_FUNCTION (this);
  return m_beta;
}

//SwiptHarvester::DoGetPower (void)
double
SwiptHarvester::DoGetPower (void) const
{
  NS_LOG_FUNCTION (this);
 
  if (m_rxing2)
  {

      double powerRecharge = (m_harvestedPower + m_plcpHeaderPowerHarvested + m_plcpPayloadPowerHarvested)*m_beta;
      Ptr<LiIonEnergySource> bat =  GetEnergySource ()-> GetObject<LiIonEnergySource> ();
      bat->IncreaseRemainingEnergy (m_energyHarvested);
      
      return powerRecharge;
  } 
  else
  {
    //std::cout << Simulator::Now().GetNanoSeconds() << ", Node:" << GetNode()->GetId() << ", DoGetPower, HarvestedPower=0, rxing="<< m_rxing2 << std::endl;
    return 0.0;
  }
}

void
SwiptHarvester::NotifyTxStartNow (Time duration, double txPowerDbm)
{
  NS_LOG_FUNCTION(this << duration << txPowerDbm);
  m_txing = true;
  m_rxing2 = false;
  m_off = false;
  m_sleep = false;
  m_maybeCca = false;
  m_switching = false;
  m_idle = false;
  Time m_dur = duration;

  //std::cout << "NotifyTx, " << Simulator::Now().GetSeconds() << "s" << ", Node:" << GetNode()->GetId () << ", txPowerDbm: " << txPowerDbm << "dBm, duration: " << duration << ", rxing="<< m_rxing2 << std::endl;
}

void
SwiptHarvester::NotifyRxStartNow (Time duration)
{
  NS_LOG_FUNCTION (this << duration << "m_rxing2 = true");

  m_rxing2 = true;
  m_off = false;
  m_txing = false;
  m_maybeCca = false;
  m_sleep = false;
  m_switching = false;
  m_idle = false;

  Time dur = duration;
  m_plcpHeaderPowerHarvested = 0;
  m_plcpPayloadPowerHarvested = 0;
  m_harvestedPower = 0.0;

  UpdateSwiptHarvester ();
}

void
SwiptHarvester::NotifyRxEndOkNow (void)
{
  NS_LOG_FUNCTION(this);
  m_harvestedPower = 0.0;
  m_plcpHeaderPowerHarvested = 0.0; 
  m_plcpPayloadPowerHarvested = 0.0;

  m_maybeCca = false;
  m_rxing2 = false;
  m_off = false;
  m_txing = false;
  m_sleep = false;
  m_switching = false;
  m_idle = true;

  //std::cout << "NotifyRxEndOkNow, " << Simulator::Now().GetSeconds() << "s" << ", Node:" << GetNode()->GetId () << ", NotifyRxEndOk " << ", rxing="<< m_rxing2 << std::endl;
}

void
SwiptHarvester::NotifyRxEndErrorNow (void)
{
  NS_LOG_FUNCTION(this);
  m_harvestedPower = 0.0;
  m_plcpHeaderPowerHarvested = 0.0; 
  m_plcpPayloadPowerHarvested = 0.0;

  m_sleep = false;
  m_maybeCca = false;
  m_rxing2 = false;
  m_off = false;
  m_txing = false;
  m_switching = false;
  m_idle = true;

  //std::cout << "NotifyRxEndErrorNow, " << Simulator::Now().GetSeconds() << "s" << ", Node:" << GetNode()->GetId () << ", NotifyRxEndError " << ", rxing="<< m_rxing2 << std::endl;
}

void
SwiptHarvester::NotifyMaybeCcaBusyStartNow (Time duration)
{
  NS_LOG_FUNCTION(this << duration);
  m_maybeCca = true;
  m_txing = false;
  m_rxing2 = false;
  m_off = false;
  m_sleep = false;
  m_switching = false;
  m_idle = false;

  Time m_dur = duration;
  m_energyHarvested = 0.0;

  //std::cout << "NotifyMaybeCcaBusyStartNow, "  << Simulator::Now().GetSeconds() << "s" << ", Node:" << GetNode()->GetId () << ", NotifyMaybeCcaBusyStart " << ", rxing="<< m_rxing2 << std::endl;
}

void
SwiptHarvester::NotifySwitchingStartNow (Time duration)
{
  NS_LOG_FUNCTION(this << duration);
  m_sleep = false;
  m_txing = false;
  m_maybeCca = false;
  m_rxing2 = false;
  m_off = false;
  m_switching = true;
  m_idle = false;

  Time m_dur = duration;


  //std::cout << "NotifySwitchingStartNow, "  << Simulator::Now().GetSeconds() << "s" << ", Node:" << GetNode()->GetId () << ", NotifySwitchingStart " << ", rxing="<< m_rxing2 << std::endl;
}

void
SwiptHarvester::NotifySleepNow (void)
{
  NS_LOG_FUNCTION(this);
  m_sleep = true;
  m_txing = false;
  m_maybeCca = false;
  m_rxing2 = false;
  m_off = false;
  m_switching = false;
  m_idle = false;

  UpdateSwiptHarvester ();

  //std::cout << "NotifySleepNow, "  << Simulator::Now().GetSeconds() << "s" << ", Node:" << GetNode()->GetId () << ", NotifySleepNow " << ", rxing="<< m_rxing2 << std::endl;
  //m_switchToIdleEvent.Cancel ();
}
 
void
SwiptHarvester::NotifyWakeupNow (void)
{
  NS_LOG_FUNCTION(this);
  m_sleep = false;
  m_rxing2 = false;
  m_off = false;
  m_txing = false;
  m_maybeCca = false;
  m_switching = false;
  m_idle = false;

  //std::cout << "NotifyWakeupNow, " << Simulator::Now().GetSeconds() << "s" << ", Node:" << GetNode()->GetId () << ", NotifyWakeupNow " << ", rxing="<< m_rxing2 << std::endl;
}

void
SwiptHarvester::NotifyPlcpHeader (double noiseInterferenceHeaderW, 
                                  Time noiseInterferenceHeaderDuration, 
                                  Ptr<SwiptHarvester> swiptH)
{
  NS_LOG_FUNCTION(this);

  if (swiptH!=NULL)
  {
    //if (GetPowerSplitFactor () != 1.0)
    if ((m_rxing2) && (m_psFactor < 0.95))
    {
      //std::cout << "NotifyPlcpHeader, psFactor: " << GetPowerSplitFactor () << std::endl;
      m_headerDuration = noiseInterferenceHeaderDuration;
      m_interferenceWHeader = std::abs (noiseInterferenceHeaderW);
      m_plcpHeaderEnergyHarvested = (m_interferenceWHeader * noiseInterferenceHeaderDuration.GetSeconds ()/GetPowerSplitFactor ()) * (1.0 -GetPowerSplitFactor ())* m_eta;
      totalInterferenceHeader += m_plcpHeaderEnergyHarvested ;

      // Update the total energy harvested
      m_totalEnergyHarvestedJ += m_plcpHeaderEnergyHarvested;

      // Calculate the Power Harvested
      m_plcpHeaderPowerHarvested = (m_plcpHeaderEnergyHarvested / noiseInterferenceHeaderDuration.GetSeconds ()) ;

      // Update the total energy harvested
      m_totalHarvestedPower += m_plcpHeaderPowerHarvested;

      //UpdateEnergySourceDepletion (m_energyHarvested);
    }
    else
    {
      m_headerDuration = Seconds (0.0);
      m_interferenceWHeader = 0.0;
      m_plcpHeaderEnergyHarvested = 0.0;
      m_plcpHeaderPowerHarvested = 0.0;
      // m_totalHarvestedPower += m_plcpHeaderPowerHarvested;
      // m_totalEnergyHarvestedJ += m_plcpHeaderEnergyHarvested;
    } 
  }                                
}

void
SwiptHarvester::NotifyPlcpPayload (double noiseInterferencePayloadW, 
                                   Time noiseInterferenceWDuration, 
                                   Ptr<SwiptHarvester> swiptH)
{
  NS_LOG_FUNCTION(this);

  if (swiptH!=NULL)
  {
    double payloadDuration;

    //if (GetPowerSplitFactor () != 1.0)
    if ((m_rxing2) && (m_psFactor < 0.95))
    {
      payloadDuration = noiseInterferenceWDuration.GetSeconds () - m_headerDuration.GetSeconds ();
      m_interferenceWPayload = std::abs (noiseInterferencePayloadW);

      m_plcpPayloadEnergyHarvested = (m_interferenceWPayload*payloadDuration/GetPowerSplitFactor ()) * (1.0 - GetPowerSplitFactor ()) * m_eta ;

      // Update the total energy harvested
      m_totalEnergyHarvestedJ += m_plcpPayloadEnergyHarvested;

      // Calculate the Power Harvested
      m_plcpPayloadPowerHarvested = (m_plcpPayloadEnergyHarvested / payloadDuration);

      // Update the total energy harvested
      m_totalHarvestedPower += m_plcpPayloadPowerHarvested;

      totalInterferencePayload += m_plcpPayloadEnergyHarvested;

      //UpdateEnergySourceDepletion (noiseInterferencePayloadJ);  
    }
    else
    {
      payloadDuration = 0.0;
      m_interferenceWPayload = 0.0;
      m_plcpPayloadEnergyHarvested = 0.0;
      m_plcpPayloadPowerHarvested = 0.0;
    }
  }                                 
}

void
SwiptHarvester::NotifyOffNow (void)
{
  NS_LOG_FUNCTION(this);
  m_off = true;
  m_rxing2 = false;
  m_sleep = false;
  m_txing = false;
  m_switching = false;
  m_maybeCca = false;
  m_idle = false;

  //std::cout  << "SwiptHarvester::NotifyOffNow " << Simulator::Now().GetSeconds() << "s" << ", Node:" << GetNode()->GetId () << ", NotifyOffNow " << ", rxing="<< m_rxing2 << std::endl;
}

void
SwiptHarvester::NotifyOnNow (void)
{
  NS_LOG_FUNCTION(this);
  m_off = false;
  m_rxing2 = false;
  m_sleep = false;
  m_txing = false;
  m_switching = false;
  m_maybeCca = false;
  //std::cout  << "SwiptHarvester::NotifyOnNow " << Simulator::Now().GetSeconds() << "s" << ", Node:" << GetNode()->GetId () << ", NotifyOnNow " << ", rxing="<< m_rxing2 << std::endl;
}

void
SwiptHarvester::NotifyIdleNow (void)
{
  NS_LOG_FUNCTION(this);
  m_off = false;
  m_rxing2 = false;
  m_sleep = false;
  m_txing = false;
  m_switching = false;
  m_maybeCca = false;
  m_idle = true;
  //std::cout  << "SwiptHarvester::NotifyOnNow " << Simulator::Now().GetSeconds() << "s" << ", Node:" << GetNode()->GetId () << ", NotifyOnNow " << ", rxing="<< m_rxing2 << std::endl;
}
 
} // namespace ns3
