import math

# Parameters
DATARATE = 150 * 1000 # bits per second
BASICRATE = 300 * 1000 # bits per second
PAYLOAD_UDP = 100 # bytes
PAYLOAD_BEACON = 70 # bytes

SLOT_COUNT = 441

SLOT_TIME = 500E-6 + SLOT_COUNT*120E-6 # seconds
SIFS_TIME = 0.000160 # seconds
DIFS_TIME = 0.000304 # seconds
DELTA = 0.0000033

PHY_HEADER_CRC_SIZE = 8 # bytes
MAC_HEADER_CRC_SIZE = 28 # bytes

BACKOFF_SLOTS = 31
CW_MIN = 15
CW_MAX = 1023
BEACON_INTERVAL = 0.1024 # seconds
NUM_RAW_GROUPS = 3
NUM_RAW_SLOTS_PER_GROUP = 1
NUM_STATIONS_PER_GROUP = 10
STATION_TX_INTERVAL = 5 # seconds
NETWORK_RUNTIME = 1200 # seconds

PREAMBLE_LENGTH = 8 # bytes
SIGNAL_EXTENSION_DURATION = 6.4e-6 # seconds
SHORT_TRAINING_FIELD_DURATION = 8e-6 # seconds
LONG_TRAINING_FIELD_DURATION = 8e-6 # seconds
RX_START_DELAY = 1e-6 # seconds
RX_SYMBOL_DURATION = 8e-6 # seconds
CCA_DURATION = 4e-6 # seconds
PHY_HEADER = 0.000192# %em segundos, parametro do ah

ACK = (14 + PHY_HEADER_CRC_SIZE)*8/BASICRATE
ACK_TIMEOUT = 2*DELTA + SIFS_TIME + ACK

PAYLOAD_UDP_DURATION = PAYLOAD_UDP*8/DATARATE
PAYLOAD_BEACON_DURATION = PAYLOAD_BEACON*8/BASICRATE

# Successfull TX mean time channel occupation
Ts = (PAYLOAD_UDP + PHY_HEADER_CRC_SIZE + MAC_HEADER_CRC_SIZE ) * 8 / (DATARATE) + SIFS_TIME + (2*DELTA) + ACK + DIFS_TIME

# Collision mean time channel occupation
Tc = (PAYLOAD_UDP + PHY_HEADER_CRC_SIZE + MAC_HEADER_CRC_SIZE ) * 8 / (DATARATE) + DIFS_TIME + DELTA + ACK_TIMEOUT

Tg = 0.000008
Th = Ts + 5*Tg
Tidle = (BEACON_INTERVAL/NUM_RAW_SLOTS_PER_GROUP)- Th

x0 = [0.01,0.01] # valor inicial para SciPy.fsolve()
fun1_slots = @solucao_ah_v5_qvariavel_n5




# Calculate durations for each state
def sleep_duration():
    #return BEACON_INTERVAL - (NUM_RAW_GROUPS * SLOT_TIME)
    return NUM_RAW_GROUPS * BEACON_INTERVAL - (NUM_RAW_SLOTS_PER_GROUP * SLOT_TIME)

def tx_duration():
    airtime = (PAYLOAD_UDP + PHY_HEADER_CRC_SIZE + MAC_HEADER_CRC_SIZE ) * 8 / (DATARATE * 1000) # seconds
    return airtime

def rx_duration():
    rx_state_duration = PREAMBLE_LENGTH * 8 / (DATARATE * 1000) + SIGNAL_EXTENSION_DURATION + \
                    SHORT_TRAINING_FIELD_DURATION + LONG_TRAINING_FIELD_DURATION + RX_START_DELAY + \
                    tx_duration + RX_SYMBOL_DURATION # seconds
    #return tx_duration() + SIFS_TIME + DIFS_TIME + (2 * SLOT_TIME)
    return rx_state_duration

def idle_duration():
    idle_state_duration = PREAMBLE_LENGTH * 8 / (DATARATE * 1000) + SIGNAL_EXTENSION_DURATION + \
                      SHORT_TRAINING_FIELD_DURATION + LONG_TRAINING_FIELD_DURATION # seconds
    return idle_state_duration

def cca_duration():
    cca_state_duration = PREAMBLE_LENGTH * 8 / (DATARATE * 1000) + SIGNAL_EXTENSION_DURATION + \
                     SHORT_TRAINING_FIELD_DURATION + CCA_DURATION # seconds
    return cca_state_duration

# Calculate contention window
def contention_window(collision_count):
    return min(CW_MAX, CW_MIN * (2 ** collision_count))

# Simulate network operation
def simulate_network():
    total_time = 0
    collision_count = 0
    contention_slots = 0
    for i in range(NETWORK_RUNTIME // STATION_TX_INTERVAL):
        for j in range(NUM_RAW_GROUPS):
            #print("RAW Group #: {}".format(j))
            for k in range(NUM_STATIONS_PER_GROUP):
                print("RAW Group #: {}".format(j), "Node id: {}".format(k))
                while True:
                    contention_slots += 1
                    print("Contention_Slots: {}".format(contention_slots))
                    print("\n")
                    contention_win = contention_window(collision_count)
                    backoff_slots = math.floor((contention_win * SLOT_TIME) / SIFS_TIME)
                    if contention_slots > backoff_slots:
                        break
                if contention_slots > 1:
                    collision_count += 1
                total_time += idle_duration()
                if j == (i % NUM_RAW_GROUPS):
                    total_time += cca_duration()
                    total_time += tx_duration()
                    total_time += SIFS_TIME
                    total_time += rx_duration()
                else:
                    total_time += sleep_duration()
        total_time += (BEACON_INTERVAL - (NUM_RAW_GROUPS * SLOT_TIME)) * NUM_RAW_GROUPS
    return total_time

# Print results
total_time = simulate_network()
print("Total network time: {} seconds".format(total_time))
print("SLEEP time: {} seconds".format(sleep_duration() * NUM_RAW_GROUPS))
print("TX time: {} seconds".format(tx_duration() * (NETWORK_RUNTIME // STATION_TX_INTERVAL) * NUM_RAW_GROUPS * NUM_STATIONS_PER_GROUP))
print("RX time: {} seconds".format(rx_duration() * (NETWORK_RUNTIME // STATION_TX_INTERVAL) * NUM_RAW_GROUPS * NUM_STATIONS_PER_GROUP))
print("IDLE time: {} seconds".format(idle_duration() * (NETWORK_RUNTIME // STATION_TX_INTERVAL) * NUM_RAW_GROUPS * NUM_STATIONS_PER_GROUP))
print("CCA time: {} seconds".format(cca_duration() * (NETWORK_RUNTIME // STATION_TX_INTERVAL) * NUM_RAW_GROUPS * NUM_STATIONS_PER_GROUP))
