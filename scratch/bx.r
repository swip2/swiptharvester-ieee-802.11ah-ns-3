bx.bar(            df10["nodeId"],          df10["consump"],   color='#1f77b4', label=r"$\theta_s$=1.0",          linewidth=3.0,   linestyle="-"       )
bx.bar(             df9["nodeId"],           df9["consump"],   color='#ff7f0e', label=r"$\theta_s$=0.9",          linewidth=3.0,   linestyle="--"      )
bx.bar(             df8["nodeId"],           df8["consump"],   color='#2ca02c', label=r"$\theta_s$=0.8",          linewidth=3.0,   linestyle="-."      )
bx.bar(             df7["nodeId"],           df7["consump"],   color='#d62728', label=r"$\theta_s$=0.7",          linewidth=3.0,   linestyle=":"       )
bx.bar(             df6["nodeId"],           df6["consump"],   color='#9467bd', label=r"$\theta_s$=0.6",          linewidth=3.0,   linestyle="solid"   )
bx.bar(             df5["nodeId"],           df5["consump"],   color='#8c564b', label=r"$\theta_s$=0.5",          linewidth=3.0,   linestyle=":"       )
bx.bar(             df4["nodeId"],           df4["consump"],   color='#e377c2', label=r"$\theta_s$=0.4",          linewidth=3.0,   linestyle="-."      )
bx.bar(             df3["nodeId"],           df3["consump"],   color='#7f7f7f', label=r"$\theta_s$=0.3",          linewidth=3.0,   linestyle="--"      )
bx.bar(             df2["nodeId"],           df2["consump"],   color='#bcbd22', label=r"$\theta_s$=0.2",          linewidth=3.0,   linestyle="-"       )
bx.bar(             df1["nodeId"],           df1["consump"],   color='#17becf', label=r"$\theta_s$=0.1",          linewidth=3.0,   linestyle="-"       )
bx.bar(           df001["nodeId"],         df001["consump"],   color='#1f77b4', label=r"$\theta_s$=$10^{-2}$",    linewidth=3.0,   linestyle="--"      )
bx.bar(          df0001["nodeId"],        df0001["consump"],   color='#ff7f0e', label=r"$\theta_s$=$10^{-3}$",    linewidth=3.0,   linestyle="-."      )
bx.bar(         df00001["nodeId"],       df00001["consump"],   color='#2ca02c', label=r"$\theta_s$=$10^{-4}$",    linewidth=3.0,   linestyle=":"       )
bx.bar(        df000001["nodeId"],      df000001["consump"],   color='#d62728', label=r"$\theta_s$=$10^{-5}$",    linewidth=3.0,   linestyle="solid"   )
bx.bar(       df0000001["nodeId"],     df0000001["consump"],   color='#9467bd', label=r"$\theta_s$=$10^{-6}$",    linewidth=3.0,   linestyle=":"       )
bx.bar(      df00000001["nodeId"],    df00000001["consump"],   color='#8c564b', label=r"$\theta_s$=$10^{-7}$",    linewidth=3.0,   linestyle="-."      )
bx.bar(     df000000001["nodeId"],   df000000001["consump"],   color='#e377c2', label=r"$\theta_s$=$10^{-8}$",    linewidth=3.0,   linestyle="--"      )
bx.bar(    df0000000001["nodeId"],  df0000000001["consump"],   color='#7f7f7f', label=r"$\theta_s$=$10^{-9}$",    linewidth=3.0,   linestyle="-"       )
bx.bar(   df00000000001["nodeId"], df00000000001["consump"],   color='#bcbd22', label=r"$\theta_s$=$10^{-10}$",   linewidth=3.0,   linestyle="-"       )
bx.bar(             df0["nodeId"],           df0["consump"],   color='#17becf', label=r"$\theta_s$=0.0",          linewidth=3.0,   linestyle="--"      )


bx.bar( node0["nodeId"],  node0["consump"],   color='#1f77b4', label=r"$\theta_s$=$10^{-5}$",    linewidth=3.0,   linestyle="-"       )
bx.bar( node1["nodeId"],  node1["consump"],   color='#1f77b4', label=r"$\theta_s$=$10^{-5}$",    linewidth=3.0,   linestyle="--"      )
bx.bar( node2["nodeId"],  node2["consump"],   color='#1f77b4', label=r"$\theta_s$=$10^{-5}$",    linewidth=3.0,   linestyle="-."      )
bx.bar( node3["nodeId"],  node3["consump"],   color='#1f77b4', label=r"$\theta_s$=$10^{-5}$",    linewidth=3.0,   linestyle=":"       )
bx.bar( node4["nodeId"],  node4["consump"],   color='#1f77b4', label=r"$\theta_s$=$10^{-5}$",    linewidth=3.0,   linestyle="solid"   )
bx.bar( node5["nodeId"],  node5["consump"],   color='#1f77b4', label=r"$\theta_s$=$10^{-5}$",    linewidth=3.0,   linestyle=":"       )
bx.bar( node6["nodeId"],  node6["consump"],   color='#1f77b4', label=r"$\theta_s$=$10^{-5}$",    linewidth=3.0,   linestyle="-."      )
bx.bar( node7["nodeId"],  node7["consump"],   color='#1f77b4', label=r"$\theta_s$=$10^{-5}$",    linewidth=3.0,   linestyle="--"      )
bx.bar( node8["nodeId"],  node8["consump"],   color='#1f77b4', label=r"$\theta_s$=$10^{-5}$",    linewidth=3.0,   linestyle="-"       )
bx.bar( node9["nodeId"],  node9["consump"],   color='#1f77b4', label=r"$\theta_s$=$10^{-5}$",    linewidth=3.0,   linestyle="-"       )
bx.bar( node10["nodeId"],node10["consump"],   color='#1f77b4', label=r"$\theta_s$=$10^{-5}$",    linewidth=3.0,   linestyle="--"      )
bx.bar( node11["nodeId"],node11["consump"],   color='#1f77b4', label=r"$\theta_s$=$10^{-5}$",    linewidth=3.0,   linestyle="-."      )
bx.bar( node12["nodeId"],node12["consump"],   color='#1f77b4', label=r"$\theta_s$=$10^{-5}$",    linewidth=3.0,   linestyle=":"       )
bx.bar( node13["nodeId"],node13["consump"],   color='#1f77b4', label=r"$\theta_s$=$10^{-5}$",    linewidth=3.0,   linestyle="solid"   )
bx.bar( node14["nodeId"],node14["consump"],   color='#1f77b4', label=r"$\theta_s$=$10^{-5}$",    linewidth=3.0,   linestyle=":"       )


df0000000001 =  df[df["psFactor"] == 0.000000001]
df000000001 =   df[df["psFactor"] == 0.00000001]
df00000001 =    df[df["psFactor"] == 0.0000001]
df0000001 =     df[df["psFactor"] == 0.000001]
df000001 =      df[df["psFactor"] == 0.00001]
df00001 =       df[df["psFactor"] == 0.0001]
df0001 =        df[df["psFactor"] == 0.0010]
df001 =         df[df["psFactor"] == 0.0100]
df1 =           df[df["psFactor"] == 0.1]
df2 =           df[df["psFactor"] == 0.2]
df3 =           df[df["psFactor"] == 0.3]
df4 =           df[df["psFactor"] == 0.4]
df5 =           df[df["psFactor"] == 0.5]
df6 =           df[df["psFactor"] == 0.6]
df7 =           df[df["psFactor"] == 0.7]
df8 =           df[df["psFactor"] == 0.8]
df9 =           df[df["psFactor"] == 0.9]
df10 =          df[df["psFactor"] == 1.0]