/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 MIRKO BANCHI
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * 	In [1]: 500e-6 + 849 * 120e-6
	Out[1]: 0.10238

	In [2]: 1500*8/(150000)
	Out[2]: 0.08

	In [3]: 0.10238/0.08
	Out[3]: 1.27975

 */

#include "s1g-test-tim-raw.h"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/config-store-module.h"
#include "ns3/wifi-module.h"
#include "ns3/internet-module.h"
#include "ns3/energy-module.h"
#include "ns3/udp-client-server-helper.h"
#include "ns3/udp-echo-helper.h"
#include "ns3/udp-echo-server.h"
#include "ns3/udp-echo-client.h"
#include "ns3/udp-server.h"
#include "ns3/udp-client.h"
#include "ns3/packet.h"
#include "ns3/on-off-helper.h"
#include "ns3/packet-sink.h"
#include "ns3/packet-sink-helper.h"
#include "ns3/command-line.h"
#include "ns3/rps.h"
#include "ns3/log.h"
#include "ns3/node.h"
#include "ns3/simulator.h"
#include "ns3/double.h"
#include "ns3/config.h"
#include "ns3/string.h"
#include "ns3/extension-headers.h"
#include "ns3/stats-module.h"
#include "ns3/application.h"
#include "ns3/flow-monitor.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/ipv4-flow-classifier.h"
#include "ns3/wifi-tx-vector.h"
#include "ns3/wifi-mode.h"
#include "ns3/mac-low.h"
#include "ns3/dca-txop.h"
#include "ns3/edca-txop-n.h"
#include "ns3/propagation-loss-model.h"
#include "ns3/buildings-propagation-loss-model.h"


#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/internet-module.h"
#include "ns3/extension-headers.h"

#include "ns3/ipv4-address.h"
#include "ns3/inet-socket-address.h"
#include "ns3/core-module.h"
#include "ns3/radiotap-header.h"
#include "ns3/flow-monitor-module.h"


#include <regex>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <iomanip>
#include <utility>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <sys/stat.h>
#include <map>

NS_LOG_COMPONENT_DEFINE("s1g-wifi-network-tim-raw");

uint32_t AssocNum = 0;
int64_t AssocTime = 0;
uint32_t StaNum = 0;
NetDeviceContainer staDeviceCont;
const int MaxSta = 8000;

uint32_t onFramesTx[31];
uint32_t onFramesRxAtNode[31];
uint32_t onFramesRxAtApFromNode[31];
uint32_t onFramesRxMonitor[31];
double g_signalDbmAvg[31];
double g_noiseDbmAvg[31]; 
double m_rxSignalDbm[31];
uint32_t NumberOfRetriesRx[31];
uint32_t NumberOfPowerBeaconsSent;


//Ptr<EdcaTxopN> m_dcf;

Configuration config;
Statistics stats;
SimulationEventManager eventManager;

Ptr<Socket> powerBeacon;
Ptr<Socket> recvSink;
TypeId tidPowerBeacon = TypeId::LookupByName("ns3::UdpSocketFactory");


TypeId tidInterference = TypeId::LookupByName("ns3::UdpSocketFactory");

std::string dropReason[] =
	{
		"Unknown",
		"PhyInSleepMode",
		"PhyNotEnoughSignalPower",
		"PhyUnsupportedMode",
		"PhyPreampleHeaderReceptionFailed",
		"PhyRxDuringChannelSwitching",
		"PhyAlreadyReceiving",
		"PhyAlreadyTransmitting",
		"PhyPlcpReceptionFailed",
		"MacNotForAP",
		"MacAPToAPFrame",
		"MacQueueDelayExceeded",
		"MacQueueSizeExceeded",
		"TCPTxBufferExceeded",
		"PhyRxNotEnoughSNIR",
		"DeviceIsOff"};

Mac48Address macAdd[] =	{
		"00:00:00:00:00:01", // 0
		"00:00:00:00:00:02", // 1
		"00:00:00:00:00:03", //2
		"00:00:00:00:00:04", //3
		"00:00:00:00:00:05", //4
		"00:00:00:00:00:06", //5
		"00:00:00:00:00:07", //6
		"00:00:00:00:00:08", //7
		"00:00:00:00:00:09", //8
		"00:00:00:00:00:0a", //9
		"00:00:00:00:00:0b", //10
		"00:00:00:00:00:0c", //11
		"00:00:00:00:00:0d", //12
		"00:00:00:00:00:0e", //13
		"00:00:00:00:00:0f", //14
		"00:00:00:00:00:10", //15
		"00:00:00:00:00:11", //16
		"00:00:00:00:00:12", //17
		"00:00:00:00:00:13", //18
		"00:00:00:00:00:14", //19
		"00:00:00:00:00:15", //20
		"00:00:00:00:00:16", //21
		"00:00:00:00:00:17", //22
		"00:00:00:00:00:18", //23
		"00:00:00:00:00:19", //24
		"00:00:00:00:00:1a", //25
		"00:00:00:00:00:1b", //26
		"00:00:00:00:00:1c", //27
		"00:00:00:00:00:1d", //28
		"00:00:00:00:00:1e", //29
		"00:00:00:00:00:1f", //30
		"00:00:00:00:00:20",
		"00:00:00:00:00:21",
		"00:00:00:00:00:22",
		"00:00:00:00:00:23",
		"00:00:00:00:00:24",
		"00:00:00:00:00:25",
		"00:00:00:00:00:26",
		"00:00:00:00:00:27",
		"00:00:00:00:00:28",
		"00:00:00:00:00:29",
		"00:00:00:00:00:2a",
		"00:00:00:00:00:2b",
		"00:00:00:00:00:2c",
		"00:00:00:00:00:2d",
		"00:00:00:00:00:2e",
		"00:00:00:00:00:2f",
		"00:00:00:00:00:30",
		"00:00:00:00:00:31",
		"00:00:00:00:00:32",
		"00:00:00:00:00:33",
		"00:00:00:00:00:34",
		"00:00:00:00:00:35",
		"00:00:00:00:00:36",
		"00:00:00:00:00:37",
		"00:00:00:00:00:38",
		"00:00:00:00:00:39",
		"00:00:00:00:00:3a",
		"00:00:00:00:00:3b",
		"00:00:00:00:00:3c",
		"00:00:00:00:00:3d",
		"00:00:00:00:00:3e",
		"00:00:00:00:00:3f",
		"00:00:00:00:00:40"};

class assoc_record
{
public:
	assoc_record();
	bool GetAssoc();
	void SetAssoc(std::string context, Mac48Address address);
	void UnsetAssoc(std::string context, Mac48Address address);
	void setstaid(uint16_t id);

private:
	bool assoc;
	uint16_t staid;
};

assoc_record::assoc_record()
{
	assoc = false;
	staid = 65535;
}

void assoc_record::setstaid(uint16_t id)
{
	staid = id;
}

void assoc_record::SetAssoc(std::string context, Mac48Address address)
{
	assoc = true;
}

void assoc_record::UnsetAssoc(std::string context, Mac48Address address)
{
	assoc = false;
}

bool assoc_record::GetAssoc()
{
	return assoc;
}

typedef std::vector<assoc_record *> assoc_recordVector;
assoc_recordVector assoc_vector;

uint32_t GetAssocNum()
{
	AssocNum = 0;
	for (assoc_recordVector::const_iterator index = assoc_vector.begin();
		 index != assoc_vector.end(); index++)
	{
		if ((*index)->GetAssoc())
		{
			AssocNum++;
		}
	}
	return AssocNum;
}

void PopulateArpCache()
{
	Ptr<ArpCache> arp = CreateObject<ArpCache>();
	arp->SetAliveTimeout(Seconds(3600 * 24 * 365));
	for (NodeList::Iterator i = NodeList::Begin(); i != NodeList::End(); ++i)
	{
		Ptr<Ipv4L3Protocol> ip = (*i)->GetObject<Ipv4L3Protocol>();
		NS_ASSERT(ip != 0);
		ObjectVectorValue interfaces;
		ip->GetAttribute("InterfaceList", interfaces);
		for (ObjectVectorValue::Iterator j = interfaces.Begin();
			 j != interfaces.End(); j++)
		{
			Ptr<Ipv4Interface> ipIface =
				(j->second)->GetObject<Ipv4Interface>();
			NS_ASSERT(ipIface != 0);
			Ptr<NetDevice> device = ipIface->GetDevice();
			NS_ASSERT(device != 0);
			Mac48Address addr = Mac48Address::ConvertFrom(device->GetAddress());
			for (uint32_t k = 0; k < ipIface->GetNAddresses(); k++)
			{
				Ipv4Address ipAddr = ipIface->GetAddress(k).GetLocal();
				if (ipAddr == Ipv4Address::GetLoopback())
					continue;
				ArpCache::Entry *entry = arp->Add(ipAddr);
				entry->MarkWaitReply(0);
				entry->MarkAlive(addr);
				std::cout << "Arp Cache: Adding the pair (" << addr << ","
						  << ipAddr << ")" << std::endl;
			}
		}
	}
	for (NodeList::Iterator i = NodeList::Begin(); i != NodeList::End(); ++i)
	{
		Ptr<Ipv4L3Protocol> ip = (*i)->GetObject<Ipv4L3Protocol>();
		NS_ASSERT(ip != 0);
		ObjectVectorValue interfaces;
		ip->GetAttribute("InterfaceList", interfaces);
		for (ObjectVectorValue::Iterator j = interfaces.Begin();
			 j != interfaces.End(); j++)
		{
			Ptr<Ipv4Interface> ipIface =
				(j->second)->GetObject<Ipv4Interface>();
			ipIface->SetAttribute("ArpCache", PointerValue(arp));
		}
	}
}

uint16_t ngroup;
uint16_t nslot;

RPSVector configureRAW(RPSVector rpslist, string RAWConfigFile) 
{
	uint16_t NRPS = 0;
	uint16_t NRAWPERBEACON = 0;
	uint16_t Value = 0;
	uint32_t page = 0;
	uint32_t aid_start = 0;
	uint32_t aid_end = 0;
	uint32_t rawinfo = 0;

	ifstream myfile(RAWConfigFile);
	//1. get info from config file

	//2. define RPS
	if (myfile.is_open()) {
		myfile >> NRPS;
		int totalNumSta = 0;
		for (int kk = 0; kk < NRPS; kk++) // number of beacons covering all raw groups
		{
			RPS *m_rps = new RPS;
			myfile >> NRAWPERBEACON;
			ngroup = NRAWPERBEACON;
			for (int i = 0; i < NRAWPERBEACON; i++) // raw groups in one beacon
			{
				//RPS *m_rps = new RPS;
				RPS::RawAssignment *m_raw = new RPS::RawAssignment;

				myfile >> Value;
				m_raw->SetRawControl(Value);  //support paged STA or not
				myfile >> Value;
				m_raw->SetSlotCrossBoundary(Value);
				myfile >> Value;
				m_raw->SetSlotFormat(Value);
				myfile >> Value;
				m_raw->SetSlotDurationCount(Value);
				myfile >> Value;
				nslot = Value;
				m_raw->SetSlotNum(Value);
				myfile >> page;
				myfile >> aid_start;
				myfile >> aid_end;
				rawinfo = (aid_end << 13) | (aid_start << 2) | page;
				m_raw->SetRawGroup(rawinfo);
				totalNumSta += aid_end - aid_start + 1;
				m_rps->SetRawAssignment(*m_raw);
				delete m_raw;
			}
			rpslist.rpsset.push_back(m_rps);
			//config.nRawGroupsPerRpsList.push_back(NRAWPERBEACON);
		}
		myfile.close();
		config.NRawSta = totalNumSta;
		//rpslist.rpsset[rpslist.rpsset.size() - 1]->GetRawAssigmentObj(
		//				NRAWPERBEACON - 1).GetRawGroupAIDEnd();
	} else
		cout << "Unable to open RAW configuration file \n";

	return rpslist;
}
/*
pageslice element and TIM(DTIM) together accomplish page slicing.

Prior knowledge:
802.11ah support up to 8192 stations, they are constructed into: page, block,
 subblock, sta.
there are 13 bit represent the AID of stations.
 AID[11-12] represent page.
 AID[6-10] represent block.
 AID[3-5] represent subblock.
 AID[0-2] represent sta.

A TIM(DTIM) element only support one page
A Page slice element only support one page

 Concept of page slicing:
 Between two DTIM beacon, there are many TIM beacons, only allow a TIM beacon include some blocks of one page is called page slice. One TIM beacon is called a page slice.
 Page slcie element specify number of page slice between two DTIM, number of blocks in each
 page slice.
 Page slice element only appears together with DTIM.

 Details:
 Page slice element also indicates AP has buffered data for which block, if a station is in that block, the station should first sleep, then wake up at coresponding page slice(TIM beacon) which includes that block.

 When station wake up at that block, it check whether AP has data for itself. If has, keep awake to receive packets and go to sleep in the next beacon.
 */

void configurePageSlice(void)
{
	config.pageS.SetPageindex(config.pageIndex);
	config.pageS.SetPagePeriod(config.pagePeriod);		  // 2 TIM groups between DTIMs
	config.pageS.SetPageSliceLen(config.pageSliceLength); // each TIM group has 1 block (2 blocks in 2 TIM groups)
	config.pageS.SetPageSliceCount(config.pageSliceCount);
	config.pageS.SetBlockOffset(config.blockOffset);
	config.pageS.SetTIMOffset(config.timOffset);
	std::cout << "pageIndex=" << (int)config.pageIndex << ", pagePeriod=" << (int)config.pagePeriod << ", pageSliceLength=" << (int)config.pageSliceLength << ", pageSliceCount=" << (int)config.pageSliceCount << ", blockOffset=" << (int)config.blockOffset << ", timOffset=" << (int)config.timOffset << std::endl;
	//  page 0
	//  8 TIM(page slice) for one page
	//  4 block (each page)
	//  8 page slice
	//  both offset are 0
}

void configureTIM(void)
{
	config.tim.SetPageIndex(config.pageIndex);
	if (config.pageSliceCount)
		config.tim.SetDTIMPeriod(config.pageSliceCount); // not necessarily the same
	else
		config.tim.SetDTIMPeriod(1);

	std::cout << "DTIM period=" << (int)config.pagePeriod << std::endl;
}

void checkRawAndTimConfiguration(void)
{
	std::cout << "Checking RAW and TIM configuration..." << std::endl;
	bool configIsCorrect = true;
	NS_ASSERT(config.rps.rpsset.size());
	// Number of page slices in a single page has to equal number of different RPS elements because
	// If #PS > #RPS, the same RPS will be used in more than 1 PS and that is wrong because
	// each PS can accommodate different AIDs (same RPS means same stations in RAWs)
	if (config.pageSliceCount)
	{
	//	NS_ASSERT (config.pagePeriod == config.rps.rpsset.size());
	}
	for (int j = 0; j < config.rps.rpsset.size(); j++)
	{
		uint32_t totalRawTime = 0;
		for (int i = 0; i < config.rps.rpsset[j]->GetNumberOfRawGroups(); i++)
		{
			totalRawTime += (120 * config.rps.rpsset[j]->GetRawAssigmentObj(i).GetSlotDurationCount() + 500) * config.rps.rpsset[j]->GetRawAssigmentObj(i).GetSlotNum();
			auto aidStart = config.rps.rpsset[j]->GetRawAssigmentObj(i).GetRawGroupAIDStart();
			auto aidEnd = config.rps.rpsset[j]->GetRawAssigmentObj(i).GetRawGroupAIDEnd();
			configIsCorrect = check(aidStart, j) && check(aidEnd, j);
			// AIDs in each RPS must comply with TIM in the following way:
			// TIM0: 1-63; TIM1: 64-127; TIM2: 128-191; ...; TIM32: 1983-2047
			// If RPS that belongs to TIM0 includes other AIDs (other than range [1-63]) configuration is incorrect
			NS_ASSERT(configIsCorrect);
		}
		NS_ASSERT(totalRawTime <= config.BeaconInterval);
	}
}
// assumes each TIM has its own beacon - doesn't need to be the case as there has to be only PageSliceCount beacons between DTIMs
bool check(uint16_t aid, uint32_t index)
{
	uint8_t block = (aid >> 6) & 0x001f;
	NS_ASSERT(config.pageS.GetPageSliceLen() > 0);
	//uint8_t toTim = (block - config.pageS.GetBlockOffset()) % config.pageS.GetPageSliceLen();
	if (index == config.pageS.GetPageSliceCount() - 1 && config.pageS.GetPageSliceCount() != 0)
	{
		// the last page slice has 32 - the rest blocks
		return (block <= 31) && (block >= index * config.pageS.GetPageSliceLen());
	}
	else if (config.pageS.GetPageSliceCount() == 0)
		return true;

	return (block >= index * config.pageS.GetPageSliceLen()) && (block < (index + 1) * config.pageS.GetPageSliceLen());
}

void sendStatistics(bool schedule)
{
	eventManager.onUpdateStatistics(stats);
	eventManager.onUpdateSlotStatistics(
		transmissionsPerTIMGroupAndSlotFromAPSinceLastInterval,
		transmissionsPerTIMGroupAndSlotFromSTASinceLastInterval);
	// reset
	std::fill(transmissionsPerTIMGroupAndSlotFromAPSinceLastInterval.begin(),
			  transmissionsPerTIMGroupAndSlotFromAPSinceLastInterval.end(), 0);
	std::fill(transmissionsPerTIMGroupAndSlotFromSTASinceLastInterval.begin(),
			  transmissionsPerTIMGroupAndSlotFromSTASinceLastInterval.end(), 0);

	if (schedule)
		Simulator::Schedule(Seconds(config.visualizerSamplingInterval), &sendStatistics, true);
}

void onSTADeassociated(int i)
{
	eventManager.onNodeDeassociated(*nodes[i]);
}

void updateNodesQueueLength()
{
	for (uint32_t i = 0; i < config.Nsta; i++)
	{
		nodes[i]->UpdateQueueLength();
		stats.get(i).EDCAQueueLength = nodes[i]->queueLength;
	}
	Simulator::Schedule(Seconds(0.5), &updateNodesQueueLength);
}

void onSTAAssociated(int i)
{
	cout << "Node " << std::to_string(i) << " is associated and has aid "
		 << nodes[i]->aId << endl;

	for (int k = 0; k < config.rps.rpsset.size(); k++)
	{
		for (int j = 0; j < config.rps.rpsset[k]->GetNumberOfRawGroups(); j++)
		{
			if (config.rps.rpsset[k]->GetRawAssigmentObj(j).GetRawGroupAIDStart() <= i + 1 && i + 1 <= config.rps.rpsset[k]->GetRawAssigmentObj(j).GetRawGroupAIDEnd())
			{
				nodes[i]->rpsIndex = k + 1;
				nodes[i]->rawGroupNumber = j + 1;
				nodes[i]->rawSlotIndex =
					nodes[i]->aId % config.rps.rpsset[k]->GetRawAssigmentObj(j).GetSlotNum() + 1;
				cout << "Node " << i << " with AID " << (int)nodes[i]->aId << " belongs to " << (int)nodes[i]->rawSlotIndex << " slot of RAW group "
				 << (int)nodes[i]->rawGroupNumber << " within the " << (int)nodes[i]->rpsIndex << " RPS." << endl;
				 
			}
		}
	}

	eventManager.onNodeAssociated(*nodes[i]);

	// RPS, Raw group and RAW slot assignment

	if (GetAssocNum() == config.Nsta)
	{
		cout << "All " << AssocNum << " stations associated at " << Simulator::Now().GetMicroSeconds() << "us, configuring clients & server" << endl;

		// association complete, start sending packets
		stats.TimeWhenEverySTAIsAssociated = Simulator::Now();

		if (config.trafficType == "udp")
		{
			std::cout << "UDP" << std::endl;
			configureUDPServer();
			configureUDPClients();
			//configurePowerBeacon();
		}
		else if (config.trafficType == "udpecho")
		{
			configureUDPEchoServer();
			configureUDPEchoClients();
		}
		else if (config.trafficType == "tcpecho")
		{
			configureTCPEchoServer();
			configureTCPEchoClients();
		}
		else if (config.trafficType == "tcppingpong")
		{
			configureTCPPingPongServer();
			configureTCPPingPongClients();
		}
		else if (config.trafficType == "tcpipcamera")
		{
			configureTCPIPCameraServer();
			configureTCPIPCameraClients();
		}
		else if (config.trafficType == "tcpfirmware")
		{
			configureTCPFirmwareServer();
			configureTCPFirmwareClients();
		}
		else if (config.trafficType == "tcpsensor")
		{
			configureTCPSensorServer();
			configureTCPSensorClients();
		}
		updateNodesQueueLength();
	}
}


void RpsIndexTrace(uint16_t oldValue, uint16_t newValue)
{
	currentRps = newValue;
	//Ptr<DcaTxop> m_dcf;
	//m_dcf->ResetCw ();
/*
	switch (currentRps)
	{
	case 1:
		for (int i=10; i<20; i++)
		{
			
			wifiStaNode.Get(i)->GetDevice(0)->GetObject<
		}
		break;
	case 2:
		for (int i=20; i<30; i++)
		{
			
		}
		break;	
	case 3:
		for (int i=0; i<10; i++)
		{
			
		}
		break;		
	default:
		break;
	}
*/
	cout << "RPS: " << newValue << " at " << Simulator::Now().GetMicroSeconds() << endl;
	if (config.filesOutput)
	{
		//std::stringstream ss;
		//ss << "./ResultColetorSwipt/subdir/RpsIndexTrace.csv";
		//static std::fstream f(ss.str().c_str(), std::ios::out);
		//f << std::fixed << Simulator::Now().GetSeconds() << ", " << currentRps << std::endl;
	}
}

void RawGroupTrace(uint8_t oldValue, uint8_t newValue)
{
	currentRawGroup = newValue;
	cout << "	group " << std::to_string(newValue) << " at " << Simulator::Now().GetMicroSeconds() << endl;
	if (config.filesOutput)
	{
		//std::stringstream ss;
		//ss << "./ResultColetorSwipt/subdir/RawGroupTrace.csv";
		//static std::fstream f(ss.str().c_str(), std::ios::out);
		//f << std::fixed << Simulator::Now().GetSeconds() << ", " << currentRawGroup << std::endl;
	}
}

void RawSlotTrace(uint8_t oldValue, uint8_t newValue)
{
	currentRawSlot = newValue;
	//cout << "		slot " << std::to_string(newValue) << " at " << Simulator::Now().GetMicroSeconds() << endl;
	//if (config.filesOutput)
	//{
	//	std::stringstream ss;
	//	ss << "./ResultColetorSwipt/subdir/RawSlotTrace.csv";
	//	static std::fstream f(ss.str().c_str(), std::ios::out);
	//	f << std::fixed << Simulator::Now().GetSeconds() << ", " << currentRawSlot << std::endl;
	//}
}

void PacketToApFromNode(Ptr<const Packet>, Mac48Address from)
{
	uint8_t mac[6];
    from.CopyTo (mac);
    uint8_t aid_l = mac[5];
    uint8_t aid_h = mac[4] & 0x1f;
    uint16_t aid = (aid_h << 8) | (aid_l << 0);

	//std::cout << Simulator::Now().GetMicroSeconds() << "us, AP RX PKT from: " << aid << std::endl;
    //NS_LOG_UNCOND ("Disassociation request from aid " << aid);

	config.NumberOfPacketsToApFromNode[aid - 1]++;
}

void configureNodes(NodeContainer &wifiStaNode, NetDeviceContainer &staDevice)
{
	cout << "Configuring STA Node trace sources..." << endl;

	for (uint32_t i = 0; i < config.Nsta; i++)
	{

		cout << "Hooking up trace sources for STA " << i << endl;

		NodeEntry *n = new NodeEntry(i, &stats, wifiStaNode.Get(i),
									 staDevice.Get(i));

		n->SetAssociatedCallback([=]
								 { onSTAAssociated(i); });
		n->SetDeassociatedCallback([=]
								   { onSTADeassociated(i); });

		nodes.push_back(n);
		// hook up Associated and Deassociated events
		Config::Connect(
			"/NodeList/" + std::to_string(i) + "/DeviceList/0/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/$ns3::StaWifiMac/Assoc",
			MakeCallback(&NodeEntry::SetAssociation, n));
		Config::Connect(
			"/NodeList/" + std::to_string(i) + "/DeviceList/0/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/$ns3::StaWifiMac/DeAssoc",
			MakeCallback(&NodeEntry::UnsetAssociation, n));
		Config::Connect(
			"/NodeList/" + std::to_string(i) + "/DeviceList/0/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/$ns3::StaWifiMac/NrOfTransmissionsDuringRAWSlot",
			MakeCallback(
				&NodeEntry::OnNrOfTransmissionsDuringRAWSlotChanged,
				n)); // not implem

		Config::Connect("/NodeList/" + std::to_string(i) + "/DeviceList/0/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/$ns3::StaWifiMac/S1gBeaconMissed", MakeCallback(&NodeEntry::OnS1gBeaconMissed, n));

		Config::Connect(
			"/NodeList/" + std::to_string(i) + "/DeviceList/0/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/$ns3::StaWifiMac/PacketDropped",
			MakeCallback(&NodeEntry::OnMacPacketDropped, n));		
		Config::Connect(
			"/NodeList/" + std::to_string(i) + "/DeviceList/0/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/$ns3::StaWifiMac/PacketDropped",
			MakeCallback(&OnMacPacketDropped2));
		Config::Connect(
			"/NodeList/" + std::to_string(i) + "/DeviceList/0/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/$ns3::StaWifiMac/Collision",
			MakeCallback(&NodeEntry::OnCollision, n));
		Config::Connect(
			"/NodeList/" + std::to_string(i) + "/DeviceList/0/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/$ns3::StaWifiMac/TransmissionWillCrossRAWBoundary",
			MakeCallback(&NodeEntry::OnTransmissionWillCrossRAWBoundary,
						 n)); //?

		// hook up TX
		Config::Connect(
			"/NodeList/" + std::to_string(i) + "/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxBegin",
			MakeCallback(&NodeEntry::OnPhyTxBegin, n));
		Config::Connect(
			"/NodeList/" + std::to_string(i) + "/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd",
			MakeCallback(&NodeEntry::OnPhyTxEnd, n));
		Config::Connect(
			"/NodeList/" + std::to_string(i) + "/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxDropWithReason",
			MakeCallback(&NodeEntry::OnPhyTxDrop, n)); //?		
		Config::Connect(
			"/NodeList/" + std::to_string(i) + "/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxDropWithReason",
			MakeCallback(&OnPhyTxDrop2)); //?

		// hook up RX
		Config::Connect(
			"/NodeList/" + std::to_string(i) + "/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxBegin",
			MakeCallback(&NodeEntry::OnPhyRxBegin, n));
		Config::Connect(
			"/NodeList/" + std::to_string(i) + "/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd",
			MakeCallback(&NodeEntry::OnPhyRxEnd, n));
		Config::Connect(
			"/NodeList/" + std::to_string(i) + "/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxDropWithReason",
			MakeCallback(&NodeEntry::OnPhyRxDrop,n));		
		Config::Connect(
			"/NodeList/" + std::to_string(i) + "/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxDropWithReason",
			MakeCallback(&OnPhyRxDrop2));

		// hook up MAC traces
		Config::Connect(
			"/NodeList/" + std::to_string(i) + "/DeviceList/0/$ns3::WifiNetDevice/RemoteStationManager/MacTxRtsFailed",
			MakeCallback(&NodeEntry::OnMacTxRtsFailed, n)); //?
		Config::Connect(
			"/NodeList/" + std::to_string(i) + "/DeviceList/0/$ns3::WifiNetDevice/RemoteStationManager/MacTxDataFailed",
			MakeCallback(&NodeEntry::OnMacTxDataFailed, n));
		Config::Connect(
			"/NodeList/" + std::to_string(i) + "/DeviceList/0/$ns3::WifiNetDevice/RemoteStationManager/MacTxFinalRtsFailed",
			MakeCallback(&NodeEntry::OnMacTxFinalRtsFailed, n)); //?
		Config::Connect(
			"/NodeList/" + std::to_string(i) + "/DeviceList/0/$ns3::WifiNetDevice/RemoteStationManager/MacTxFinalDataFailed",
			MakeCallback(&NodeEntry::OnMacTxFinalDataFailed, n)); //?

		// hook up PHY State change
		Config::Connect(
			"/NodeList/" + std::to_string(i) + "/DeviceList/0/$ns3::WifiNetDevice/Phy/State/State",
			MakeCallback(&NodeEntry::OnPhyStateChange, n));
	}
}

int getBandwidth(string dataMode)
{
	if (dataMode == "MCS1_0" || dataMode == "MCS1_1" || dataMode == "MCS1_2" || dataMode == "MCS1_3" || dataMode == "MCS1_4" || dataMode == "MCS1_5" || dataMode == "MCS1_6" || dataMode == "MCS1_7" || dataMode == "MCS1_8" || dataMode == "MCS1_9" || dataMode == "MCS1_10")
		return 1;

	else if (dataMode == "MCS2_0" || dataMode == "MCS2_1" || dataMode == "MCS2_2" || dataMode == "MCS2_3" || dataMode == "MCS2_4" || dataMode == "MCS2_5" || dataMode == "MCS2_6" || dataMode == "MCS2_7" || dataMode == "MCS2_8")
		return 2;

	return 0;
}

string getWifiMode(string dataMode)
{
	if (dataMode == "MCS1_0")
		return "OfdmRate300KbpsBW1MHz";
	else if (dataMode == "MCS1_1")
		return "OfdmRate600KbpsBW1MHz";
	else if (dataMode == "MCS1_2")
		return "OfdmRate900KbpsBW1MHz";
	else if (dataMode == "MCS1_3")
		return "OfdmRate1_2MbpsBW1MHz";
	else if (dataMode == "MCS1_4")
		return "OfdmRate1_8MbpsBW1MHz";
	else if (dataMode == "MCS1_5")
		return "OfdmRate2_4MbpsBW1MHz";
	else if (dataMode == "MCS1_6")
		return "OfdmRate2_7MbpsBW1MHz";
	else if (dataMode == "MCS1_7")
		return "OfdmRate3MbpsBW1MHz";
	else if (dataMode == "MCS1_8")
		return "OfdmRate3_6MbpsBW1MHz";
	else if (dataMode == "MCS1_9")
		return "OfdmRate4MbpsBW1MHz";
	else if (dataMode == "MCS1_10")
		return "OfdmRate150KbpsBW1MHz";

	else if (dataMode == "MCS2_0")
		return "OfdmRate650KbpsBW2MHz";
	else if (dataMode == "MCS2_1")
		return "OfdmRate1_3MbpsBW2MHz";
	else if (dataMode == "MCS2_2")
		return "OfdmRate1_95MbpsBW2MHz";
	else if (dataMode == "MCS2_3")
		return "OfdmRate2_6MbpsBW2MHz";
	else if (dataMode == "MCS2_4")
		return "OfdmRate3_9MbpsBW2MHz";
	else if (dataMode == "MCS2_5")
		return "OfdmRate5_2MbpsBW2MHz";
	else if (dataMode == "MCS2_6")
		return "OfdmRate5_85MbpsBW2MHz";
	else if (dataMode == "MCS2_7")
		return "OfdmRate6_5MbpsBW2MHz";
	else if (dataMode == "MCS2_8")
		return "OfdmRate7_8MbpsBW2MHz";
	return "";
}

void OnAPPhyRxDrop(std::string context, Ptr<const Packet> packet,
				   DropReason reason)
{
	// THIS REQUIRES PACKET METADATA ENABLE!
	auto pCopy = packet->Copy();
	auto it = pCopy->BeginItem();
	while (it.HasNext())
	{

		auto item = it.Next();
		Callback<ObjectBase *> constructor = item.tid.GetConstructor();

		ObjectBase *instance = constructor();
		Chunk *chunk = dynamic_cast<Chunk *>(instance);
		chunk->Deserialize(item.current);

		if (dynamic_cast<WifiMacHeader *>(chunk))
		{
			WifiMacHeader *hdr = (WifiMacHeader *)chunk;


			int staId = -1;
			if (!config.useV6)
			{
				for (uint32_t i = 0; i < staNodeInterface.GetN(); i++)
				{
					if (wifiStaNode.Get(i)->GetDevice(0)->GetAddress() == hdr->GetAddr2())
					{
						staId = i;
						break;
					}
				}
			}
			else
			{
				for (uint32_t i = 0; i < staNodeInterface6.GetN(); i++)
				{
					if (wifiStaNode.Get(i)->GetDevice(0)->GetAddress() == hdr->GetAddr2())
					{
						staId = i;
						break;
					}
				}
			}
			if (staId != -1)
			{
				stats.get(staId).NumberOfDropsByReasonAtAP[reason]++;
			}
			delete chunk;
			break;
		}
		else
			delete chunk;
	}
}

void OnAPPacketToTransmitReceived(string context, Ptr<const Packet> packet,
								  Mac48Address to, bool isScheduled, bool isDuringSlotOfSTA,
								  Time timeLeftInSlot)
{
	int staId = -1;
	if (!config.useV6)
	{
		for (uint32_t i = 0; i < staNodeInterface.GetN(); i++)
		{
			if (wifiStaNode.Get(i)->GetDevice(0)->GetAddress() == to)
			{
				staId = i;
				break;
			}
		}
	}
	else
	{
		for (uint32_t i = 0; i < staNodeInterface6.GetN(); i++)
		{
			if (wifiStaNode.Get(i)->GetDevice(0)->GetAddress() == to)
			{
				staId = i;
				break;
			}
		}
	}
	if (staId != -1)
	{
		if (isScheduled)
			stats.get(staId).NumberOfAPScheduledPacketForNodeInNextSlot++;
		else
		{
			stats.get(staId).NumberOfAPSentPacketForNodeImmediately++;
			stats.get(staId).APTotalTimeRemainingWhenSendingPacketInSameSlot +=
				timeLeftInSlot;
		}
	}
}

void onChannelTransmission(Ptr<NetDevice> senderDevice, Ptr<Packet> packet)
{
	int rpsIndex = currentRps - 1;
	int rawGroup = currentRawGroup - 1;
	int slotIndex = currentRawSlot - 1;
	//cout << rpsIndex << "		" << rawGroup << "		" << slotIndex << "		" << endl;

	uint64_t iSlot = slotIndex;
	if (rpsIndex > 0)
		for (int r = rpsIndex - 1; r >= 0; r--)
			for (int g = 0; g < config.rps.rpsset[r]->GetNumberOfRawGroups(); g++)
				iSlot += config.rps.rpsset[r]->GetRawAssigmentObj(g).GetSlotNum();

	if (rawGroup > 0)
		for (int i = rawGroup - 1; i >= 0; i--)
			iSlot += config.rps.rpsset[rpsIndex]->GetRawAssigmentObj(i).GetSlotNum();

	if (rpsIndex >= 0 && rawGroup >= 0 && slotIndex >= 0)
	{
		if (senderDevice->GetAddress() == apDevice.Get(0)->GetAddress())
		{
			// from AP
			transmissionsPerTIMGroupAndSlotFromAPSinceLastInterval[iSlot] += packet->GetSerializedSize();
		}
		else
		{
			// from STA
			transmissionsPerTIMGroupAndSlotFromSTASinceLastInterval[iSlot] += packet->GetSerializedSize();
		}
	}
	std::cout << "------------- packetSerializedSize = " << packet->GetSerializedSize() << std::endl;
	std::cout << "------------- txAP[" << iSlot <<"] = " << transmissionsPerTIMGroupAndSlotFromAPSinceLastInterval[iSlot] << std::endl;
	std::cout << "------------- txSTA[" << iSlot <<"] = " << transmissionsPerTIMGroupAndSlotFromSTASinceLastInterval[iSlot] << std::endl;
}

int getSTAIdFromAddress(Ipv4Address from)
{
	int staId = -1;
	for (int i = 0; i < staNodeInterface.GetN(); i++)
	{
		if (staNodeInterface.GetAddress(i) == from)
		{
			staId = i;
			break;
		}
	}
	return staId;
}

void udpPacketReceivedAtServer(Ptr<const Packet> packet, Address from)
{ // works
	// cout << "+++++++++++udpPacketReceivedAtServer" << endl;
	int staId = getSTAIdFromAddress(
		InetSocketAddress::ConvertFrom(from).GetIpv4());
	if (staId != -1)
		nodes[staId]->OnUdpPacketReceivedAtAP(packet);
	else
		cout << "*** Node could not be determined from received packet at AP "
			 << endl;
}

void tcpPacketReceivedAtServer(Ptr<const Packet> packet, Address from)
{
	int staId = getSTAIdFromAddress(
		InetSocketAddress::ConvertFrom(from).GetIpv4());
	if (staId != -1)
		nodes[staId]->OnTcpPacketReceivedAtAP(packet);
	else
		cout << "*** Node could not be determined from received packet at AP "
			 << endl;
}

void tcpRetransmissionAtServer(Address to)
{
	int staId = getSTAIdFromAddress(Ipv4Address::ConvertFrom(to));
	if (staId != -1)
		nodes[staId]->OnTcpRetransmissionAtAP();
	else
		cout << "*** Node could not be determined from received packet at AP "
			 << endl;
}

void tcpPacketDroppedAtServer(Address to, Ptr<Packet> packet,
							  DropReason reason)
{
	int staId = getSTAIdFromAddress(Ipv4Address::ConvertFrom(to));
	if (staId != -1)
	{
		stats.get(staId).NumberOfDropsByReasonAtAP[reason]++;
	}
}

void tcpStateChangeAtServer(TcpSocket::TcpStates_t oldState,
							TcpSocket::TcpStates_t newState, Address to)
{

	int staId = getSTAIdFromAddress(
		InetSocketAddress::ConvertFrom(to).GetIpv4());
	if (staId != -1)
		nodes[staId]->OnTcpStateChangedAtAP(oldState, newState);
	else
		cout << "*** Node could not be determined from received packet at AP "
			 << endl;

	// cout << Simulator::Now().GetMicroSeconds() << " ********** TCP SERVER SOCKET STATE CHANGED FROM " << oldState << " TO " << newState << endl;
}

void tcpIPCameraDataReceivedAtServer(Address from, uint16_t nrOfBytes)
{
	int staId = getSTAIdFromAddress(
		InetSocketAddress::ConvertFrom(from).GetIpv4());
	if (staId != -1)
		nodes[staId]->OnTcpIPCameraDataReceivedAtAP(nrOfBytes);
	else
		cout << "*** Node could not be determined from received packet at AP "
			 << endl;
}

void GenerateTraffic(Ptr<Socket> socket, uint32_t pktSize, uint32_t pktCount, Time pktInterval)
{
	if (pktCount > 0)
	{
		//stats.get(3).NumberOfSentPackets++;
		socket->Send(Create<Packet>(pktSize));
		// cout << "\n\nGenerateTraffic: " << socket << ", "
		//								<< pktSize << ", "
		//								<< pktCount << ", "
		//							  	<< Simulator::Now().GetNanoSeconds() << "ns, "
		//								<< pktInterval << endl;

		Simulator::Schedule(pktInterval, &GenerateTraffic,
							socket, pktSize, pktCount - 1, pktInterval);
	}
	else
	{
		socket->Close();
	}
}

void configureUDPServer()
{
	UdpServerHelper myServer(9);
	serverApp = myServer.Install(wifiApNode);
	serverApp.Get(0)->TraceConnectWithoutContext("Rx",
												 MakeCallback(&udpPacketReceivedAtServer));
	serverApp.Start(Seconds(0));

}


void configureUDPEchoServer()
{
	UdpEchoServerHelper myServer(9);
	serverApp = myServer.Install(wifiApNode);
	serverApp.Get(0)->TraceConnectWithoutContext("Rx",
												 MakeCallback(&udpPacketReceivedAtServer));
	serverApp.Start(Seconds(0));
}

void configureTCPEchoServer()
{
	TcpEchoServerHelper myServer(80);
	serverApp = myServer.Install(wifiApNode);
	wireTCPServer(serverApp);
	serverApp.Start(Seconds(0));
}

void configureTCPPingPongServer()
{
	// TCP ping pong is a test for the new base tcp-client and tcp-server applications
	ObjectFactory factory;
	factory.SetTypeId(TCPPingPongServer::GetTypeId());
	factory.Set("Port", UintegerValue(81));

	Ptr<Application> tcpServer = factory.Create<TCPPingPongServer>();
	wifiApNode.Get(0)->AddApplication(tcpServer);

	auto serverApp = ApplicationContainer(tcpServer);
	wireTCPServer(serverApp);
	serverApp.Start(Seconds(0));
}

void configureTCPPingPongClients()
{

	ObjectFactory factory;
	factory.SetTypeId(TCPPingPongClient::GetTypeId());
	factory.Set("Interval", TimeValue(MilliSeconds(config.trafficInterval)));
	factory.Set("PacketSize", UintegerValue(config.payloadSize));

	factory.Set("RemoteAddress",
				Ipv4AddressValue(apNodeInterface.GetAddress(0)));
	factory.Set("RemotePort", UintegerValue(81));

	Ptr<UniformRandomVariable> m_rv = CreateObject<UniformRandomVariable>();

	for (uint16_t i = 0; i < config.Nsta; i++)
	{

		Ptr<Application> tcpClient = factory.Create<TCPPingPongClient>();
		wifiStaNode.Get(i)->AddApplication(tcpClient);
		auto clientApp = ApplicationContainer(tcpClient);
		wireTCPClient(clientApp, i);

		double random = m_rv->GetValue(0, config.trafficInterval);
		clientApp.Start(MilliSeconds(0 + random));
		// clientApp.Stop(Seconds(simulationTime + 1));
	}
}

void configureTCPIPCameraServer()
{
	ObjectFactory factory;
	factory.SetTypeId(TCPIPCameraServer::GetTypeId());
	factory.Set("Port", UintegerValue(82));

	Ptr<Application> tcpServer = factory.Create<TCPIPCameraServer>();
	wifiApNode.Get(0)->AddApplication(tcpServer);

	auto serverApp = ApplicationContainer(tcpServer);
	wireTCPServer(serverApp);
	serverApp.Start(Seconds(0));
	//	serverApp.Stop(Seconds(config.simulationTime));
}

void configureTCPIPCameraClients()
{

	ObjectFactory factory;
	factory.SetTypeId(TCPIPCameraClient::GetTypeId());
	factory.Set("MotionPercentage",
				DoubleValue(config.ipcameraMotionPercentage));
	factory.Set("MotionDuration",
				TimeValue(Seconds(config.ipcameraMotionDuration)));
	factory.Set("DataRate", UintegerValue(config.ipcameraDataRate));

	factory.Set("PacketSize", UintegerValue(config.payloadSize));

	factory.Set("RemoteAddress",
				Ipv4AddressValue(apNodeInterface.GetAddress(0)));
	factory.Set("RemotePort", UintegerValue(82));

	Ptr<UniformRandomVariable> m_rv = CreateObject<UniformRandomVariable>();

	for (uint16_t i = 0; i < config.Nsta; i++)
	{

		Ptr<Application> tcpClient = factory.Create<TCPIPCameraClient>();
		wifiStaNode.Get(i)->AddApplication(tcpClient);
		auto clientApp = ApplicationContainer(tcpClient);
		wireTCPClient(clientApp, i);

		clientApp.Start(MilliSeconds(0));
		// clientApp.Stop(Seconds(config.simulationTime));
	}
}

void configureTCPFirmwareServer()
{
	ObjectFactory factory;
	factory.SetTypeId(TCPFirmwareServer::GetTypeId());
	factory.Set("Port", UintegerValue(83));

	factory.Set("FirmwareSize", UintegerValue(config.firmwareSize));
	factory.Set("BlockSize", UintegerValue(config.firmwareBlockSize));
	factory.Set("NewUpdateProbability",
				DoubleValue(config.firmwareNewUpdateProbability));

	Ptr<Application> tcpServer = factory.Create<TCPFirmwareServer>();
	wifiApNode.Get(0)->AddApplication(tcpServer);

	auto serverApp = ApplicationContainer(tcpServer);
	wireTCPServer(serverApp);
	serverApp.Start(Seconds(0));
	//	serverApp.Stop(Seconds(config.simulationTime));
}

void configureTCPFirmwareClients()
{

	ObjectFactory factory;
	factory.SetTypeId(TCPFirmwareClient::GetTypeId());
	factory.Set("CorruptionProbability",
				DoubleValue(config.firmwareCorruptionProbability));
	factory.Set("VersionCheckInterval",
				TimeValue(MilliSeconds(config.firmwareVersionCheckInterval)));
	factory.Set("PacketSize", UintegerValue(config.payloadSize));

	factory.Set("RemoteAddress",
				Ipv4AddressValue(apNodeInterface.GetAddress(0)));
	factory.Set("RemotePort", UintegerValue(83));

	Ptr<UniformRandomVariable> m_rv = CreateObject<UniformRandomVariable>();

	for (uint16_t i = 0; i < config.Nsta; i++)
	{

		Ptr<Application> tcpClient = factory.Create<TCPFirmwareClient>();
		wifiStaNode.Get(i)->AddApplication(tcpClient);
		auto clientApp = ApplicationContainer(tcpClient);
		wireTCPClient(clientApp, i);

		double random = m_rv->GetValue(0, config.trafficInterval);
		clientApp.Start(MilliSeconds(0 + random));
		clientApp.Stop(Seconds(config.simulationTime));
	}
}

void configureTCPSensorServer()
{
	ObjectFactory factory;
	factory.SetTypeId(TCPSensorServer::GetTypeId());
	factory.Set("Port", UintegerValue(84));

	Ptr<Application> tcpServer = factory.Create<TCPSensorServer>();
	wifiApNode.Get(0)->AddApplication(tcpServer);

	auto serverApp = ApplicationContainer(tcpServer);
	wireTCPServer(serverApp);
	serverApp.Start(Seconds(0));
	//	serverApp.Stop(Seconds(config.simulationTime));
}

void configureTCPSensorClients()
{

	ObjectFactory factory;
	factory.SetTypeId(TCPSensorClient::GetTypeId());

	factory.Set("Interval", TimeValue(MilliSeconds(config.trafficInterval)));
	factory.Set("PacketSize", UintegerValue(config.payloadSize));
	factory.Set("MeasurementSize", UintegerValue(config.sensorMeasurementSize));

	factory.Set("RemoteAddress",
				Ipv4AddressValue(apNodeInterface.GetAddress(0)));
	factory.Set("RemotePort", UintegerValue(84));

	Ptr<UniformRandomVariable> m_rv = CreateObject<UniformRandomVariable>();

	for (uint16_t i = 0; i < config.Nsta; i++)
	{

		Ptr<Application> tcpClient = factory.Create<TCPSensorClient>();
		wifiStaNode.Get(i)->AddApplication(tcpClient);
		auto clientApp = ApplicationContainer(tcpClient);
		wireTCPClient(clientApp, i);

		double random = m_rv->GetValue(0, config.trafficInterval);
		//clientApp.Start(MilliSeconds(0 + random));
		clientApp.Start(MilliSeconds(1));
		clientApp.Stop(Seconds(config.simulationTime));
	}
}

void wireTCPServer(ApplicationContainer serverApp)
{
	serverApp.Get(0)->TraceConnectWithoutContext("Rx",
												 MakeCallback(&tcpPacketReceivedAtServer));
	serverApp.Get(0)->TraceConnectWithoutContext("Retransmission",
												 MakeCallback(&tcpRetransmissionAtServer));
	serverApp.Get(0)->TraceConnectWithoutContext("PacketDropped",
												 MakeCallback(&tcpPacketDroppedAtServer));
	serverApp.Get(0)->TraceConnectWithoutContext("TCPStateChanged",
												 MakeCallback(&tcpStateChangeAtServer));

	if (config.trafficType == "tcpipcamera")
	{
		serverApp.Get(0)->TraceConnectWithoutContext("DataReceived",
													 MakeCallback(&tcpIPCameraDataReceivedAtServer));
	}
}

void wireTCPClient(ApplicationContainer clientApp, int i)
{

	clientApp.Get(0)->TraceConnectWithoutContext("Tx",
												 MakeCallback(&NodeEntry::OnTcpPacketSent, nodes[i]));
	clientApp.Get(0)->TraceConnectWithoutContext("Rx",
												 MakeCallback(&NodeEntry::OnTcpEchoPacketReceived, nodes[i]));

	clientApp.Get(0)->TraceConnectWithoutContext("CongestionWindow",
												 MakeCallback(&NodeEntry::OnTcpCongestionWindowChanged, nodes[i]));
	clientApp.Get(0)->TraceConnectWithoutContext("RTO",
												 MakeCallback(&NodeEntry::OnTcpRTOChanged, nodes[i]));
	clientApp.Get(0)->TraceConnectWithoutContext("RTT",
												 MakeCallback(&NodeEntry::OnTcpRTTChanged, nodes[i]));
	clientApp.Get(0)->TraceConnectWithoutContext("SlowStartThreshold",
												 MakeCallback(&NodeEntry::OnTcpSlowStartThresholdChanged, nodes[i]));
	clientApp.Get(0)->TraceConnectWithoutContext("EstimatedBW",
												 MakeCallback(&NodeEntry::OnTcpEstimatedBWChanged, nodes[i]));

	clientApp.Get(0)->TraceConnectWithoutContext("TCPStateChanged",
												 MakeCallback(&NodeEntry::OnTcpStateChanged, nodes[i]));
	clientApp.Get(0)->TraceConnectWithoutContext("Retransmission",
												 MakeCallback(&NodeEntry::OnTcpRetransmission, nodes[i]));

	clientApp.Get(0)->TraceConnectWithoutContext("PacketDropped",
												 MakeCallback(&NodeEntry::OnTcpPacketDropped, nodes[i]));

	if (config.trafficType == "tcpfirmware")
	{
		clientApp.Get(0)->TraceConnectWithoutContext("FirmwareUpdated",
													 MakeCallback(&NodeEntry::OnTcpFirmwareUpdated, nodes[i]));
	}
	else if (config.trafficType == "tcpipcamera")
	{
		clientApp.Get(0)->TraceConnectWithoutContext("DataSent",
													 MakeCallback(&NodeEntry::OnTcpIPCameraDataSent, nodes[i]));
		clientApp.Get(0)->TraceConnectWithoutContext("StreamStateChanged",
													 MakeCallback(&NodeEntry::OnTcpIPCameraStreamStateChanged,
																  nodes[i]));
	}
}

void configureTCPEchoClients()
{
	TcpEchoClientHelper clientHelper(apNodeInterface.GetAddress(0), 80); // address of remote node
	clientHelper.SetAttribute("MaxPackets", UintegerValue(4294967295u));
	clientHelper.SetAttribute("Interval",
							  TimeValue(MilliSeconds(config.trafficInterval)));
	// clientHelper.SetAttribute("IntervalDeviation", TimeValue(MilliSeconds(config.trafficIntervalDeviation)));
	clientHelper.SetAttribute("PacketSize", UintegerValue(config.payloadSize));

	Ptr<UniformRandomVariable> m_rv = CreateObject<UniformRandomVariable>();

	for (uint16_t i = 0; i < config.Nsta; i++)
	{
		ApplicationContainer clientApp = clientHelper.Install(
			wifiStaNode.Get(i));
		wireTCPClient(clientApp, i);

		double random = m_rv->GetValue(0, config.trafficInterval);
		clientApp.Start(MilliSeconds(0 + random));
		// clientApp.Stop(Seconds(simulationTime + 1));
	}
}

void configureUDPClients()
{
	// Application start time
	//Ptr<UniformRandomVariable> m_rv = CreateObject<UniformRandomVariable>();

	UdpClientHelper myClient(apNodeInterface.GetAddress(0), 9); // address of remote node
	myClient.SetAttribute("MaxPackets", config.maxNumberOfPackets);
	myClient.SetAttribute("PacketSize", UintegerValue(config.payloadSize));
	traffic_sta.clear();
	ifstream trafficfile(config.TrafficPath);
	if (trafficfile.is_open())
	{
		uint16_t sta_id;
		float sta_traffic;
		for (uint16_t kk = 0; kk < config.Nsta; kk++)
		{
			trafficfile >> sta_id;
			trafficfile >> sta_traffic;
			traffic_sta.insert(std::make_pair(sta_id, sta_traffic)); // insert data
			//cout << "sta_id = " << sta_id << " sta_traffic = " << sta_traffic << "\n";
		}
		trafficfile.close();
	}
	else
		cout << "Unable to open traffic file \n";

	//double randomStart = 0.0;
	//for (std::map<uint16_t, float>::iterator it = traffic_sta.begin(); it != traffic_sta.end(); ++it)
	for (int i=0; i< config.Nsta; i++)
	{

		myClient.SetAttribute("Interval", TimeValue(MicroSeconds(config.udpInterval))); // TODO add to nodeEntry and visualize

		Ptr<UniformRandomVariable> m_rv = CreateObject<UniformRandomVariable>();
		double randomStart = 0.0;
		randomStart = m_rv->GetValue(0.0, 1.0);
		ApplicationContainer clientApp = myClient.Install( wifiStaNode.Get(i));
		clientApp.Get(0)->TraceConnectWithoutContext("Tx", MakeCallback(&NodeEntry::OnUdpPacketSent, nodes[i]));
		clientApp.Start(Seconds(3.0 + randomStart));

	}
	AppStartTime = Simulator::Now().GetSeconds() + 1;

}

void configureUDPEchoClients()
{
	UdpEchoClientHelper clientHelper(apNodeInterface.GetAddress(0), 9); // address of remote node
	clientHelper.SetAttribute("MaxPackets", UintegerValue(4294967295u));
	clientHelper.SetAttribute("Interval", TimeValue(MilliSeconds(config.trafficInterval)));
	clientHelper.SetAttribute("PacketSize", UintegerValue(config.payloadSize));

	Ptr<UniformRandomVariable> m_rv = CreateObject<UniformRandomVariable>();

	for (uint16_t i = 0; i < config.Nsta; i++)
	{
		ApplicationContainer clientApp = clientHelper.Install(
			wifiStaNode.Get(i));
		clientApp.Get(0)->TraceConnectWithoutContext("Tx",
													 MakeCallback(&NodeEntry::OnUdpPacketSent, nodes[i]));
		clientApp.Get(0)->TraceConnectWithoutContext("Rx",
													 MakeCallback(&NodeEntry::OnUdpEchoPacketReceived, nodes[i]));

		double random = m_rv->GetValue(0, config.trafficInterval);
		clientApp.Start(Seconds(5 + random));
		// clientApp.Stop(Seconds(simulationTime + 1));
	}
}

Time timeIdleArray[MaxSta];
Time timeRxArray[MaxSta];
Time timeTxArray[MaxSta];
Time timeSleepArray[MaxSta];
Time timeCollisionArray[MaxSta];

Time timeIdleNotAssociated[MaxSta];
Time timeRxNotAssociated[MaxSta];
Time timeTxNotAssociated[MaxSta];
Time timeSleepNotAssociated[MaxSta];
Time timeCollisionNotAssociated[MaxSta];

double dist[MaxSta];

// it prints the information regarding the state of the device
void PhyStateTrace(std::string context, Time start, Time duration,
				   enum WifiPhy::State state)
{

	/*Get the number of the node from the context*/
	/*context = "/NodeList/"+strSTA+"/DeviceList/'*'/Phy/$ns3::YansWifiPhy/State/State"*/
	unsigned first = context.find("t/");
	unsigned last = context.find("/D");
	string strNew = context.substr((first + 2), (last - first - 2));

	int node = std::stoi(strNew);

	if (nodes[node]->isAssociated)
	{
		switch (state)
		{
		case WifiPhy::State::SLEEP: // Sleep
			timeSleepArray[node] = timeSleepArray[node] + duration;
			// NS_LOG_UNCOND(to_string(node + 1) + ",SLEEP," + to_string(start.GetMicroSeconds()) + " " + to_string(duration.GetMicroSeconds()));
			break;
		case WifiPhy::State::IDLE: // Idle
			timeIdleArray[node] = timeIdleArray[node] + duration;
			// NS_LOG_UNCOND(to_string(node + 1) + ",IDLE," + to_string(start.GetMicroSeconds()) + " " + to_string(duration.GetMicroSeconds()));
			break;
		case WifiPhy::State::TX: // Tx
			timeTxArray[node] = timeTxArray[node] + duration;
			// NS_LOG_UNCOND (to_string(node+1) + ",TX," + to_string(start.GetMicroSeconds()) + " " + to_string(duration.GetMicroSeconds()));
			break;
		case WifiPhy::State::RX: // Rx
			timeRxArray[node] = timeRxArray[node] + duration;
			// NS_LOG_UNCOND (to_string(node+1) + ",RX," + to_string(start.GetMicroSeconds()) + " " + to_string(duration.GetMicroSeconds()));
			break;
		case WifiPhy::State::CCA_BUSY: // CCA_BUSY
			timeCollisionArray[node] = timeCollisionArray[node] + duration;
			// NS_LOG_UNCOND (to_string(node+1) + ",CCA_BUSY," + to_string(start.GetMicroSeconds()) + " " + to_string(duration.GetMicroSeconds()));
			break;
		}
	}
	else
	{

		switch (state)
		{
		case WifiPhy::State::SLEEP: // Sleep
			timeSleepNotAssociated[node] = timeSleepNotAssociated[node] + duration;
			// NS_LOG_UNCOND(to_string(node + 1) + ",SLEEP," + to_string(start.GetMicroSeconds()) + " " + to_string(duration.GetMicroSeconds()));
			break;
		case WifiPhy::State::IDLE: // Idle
			timeIdleNotAssociated[node] = timeIdleNotAssociated[node] + duration;
			// NS_LOG_UNCOND(to_string(node + 1) + ",IDLE," + to_string(start.GetMicroSeconds()) + " " + to_string(duration.GetMicroSeconds()));
			break;
		case WifiPhy::State::TX: // Tx
			timeTxNotAssociated[node] = timeTxNotAssociated[node] + duration;
			// NS_LOG_UNCOND (to_string(node+1) + ",TX," + to_string(start.GetMicroSeconds()) + " " + to_string(duration.GetMicroSeconds()));
			break;
		case WifiPhy::State::RX: // Rx
			timeRxNotAssociated[node] = timeRxNotAssociated[node] + duration;
			// NS_LOG_UNCOND (to_string(node+1) + ",RX," + to_string(start.GetMicroSeconds()) + " " + to_string(duration.GetMicroSeconds()));
			break;
		case WifiPhy::State::CCA_BUSY: // CCA_BUSY
			timeCollisionNotAssociated[node] = timeCollisionNotAssociated[node] + duration;
			// NS_LOG_UNCOND (to_string(node+1) + ",CCA_BUSY," + to_string(start.GetMicroSeconds()) + " " + to_string(duration.GetMicroSeconds()));
			break;
		}
	}
}


template <int node>
void RemainingEnergy(double oldValue, double newValue)
{
	if (config.filesOutput)
	{
		std::stringstream ss;
		ss << "./ResultColetorSwipt/subdir/remaining_energy_" << node << ".csv";
		// ss << "./ResultColetorSwipt/subdir/battery_remaining_energy_0.csv";
		static std::fstream f(ss.str().c_str(), std::ios::out);
		// f << std::fixed << std::setprecision(9) << Simulator::Now().GetSeconds() << ", " << oldValue - newValue << ", " << newValue << ", " << node << std::endl;
		f << Simulator::Now().GetSeconds() << ", " << node << ", " << config.psFactor << ", " << oldValue - newValue << ", " << newValue << std::endl;
		// std::cout << Simulator::Now().GetSeconds () << "s, battery energy update=" << oldValue - newValue << "J, remaining energy=" << newValue << "J, node=" << node << std::endl;
	}
}

/// Trace function for total energy consumption at node.
template <int node>
void TotalEnergy(double oldValue, double newValue)
{
	double dista;
	if (node<10)
	{
		dista = 1.0;
	}
	else if ((node>=10) && (node<20))
	{
		dista = 1.75;
	}
	else if ((node>=20) && (node<30))
	{
		dista = 2.5;
	}	
	//else if ((node >= 30) && (node < 40))
	//{
	//	dista = 5.0;
	//}	
	//else if ((node >= 40) && (node < 50))
	//{
	//	dista = 6.0;
	//}	
	//else if ((node >= 50) && (node < config.Nsta))
	//{
	//	dista = 7.0;
	//}

	config.consump[node] = std::abs(oldValue);
	if (config.filesOutput)
	{
		std::stringstream ss;
		ss << "./ResultColetorSwipt/subdir/energy_consumption_" << node << ".csv";
		// ss << "./ResultColetorSwipt/subdir/energy_consumption_0.csv";
		static std::fstream f(ss.str().c_str(), std::ios::out);
		// f << std::fixed << std::setprecision(9) << Simulator::Now().GetSeconds() << ", " << newValue - oldValue << ", " << newValue << ", " << node << std::endl;
		f << Simulator::Now().GetSeconds() << ", " << node << ", " << dista << ", " << config.psFactor << ", " << newValue - oldValue << ", " << newValue << std::endl;
		// std::cout << std::fixed << std::setprecision(9) << Simulator::Now().GetSeconds() << "s, energy consumed by WifiRadio=" << newValue << "J" << std::endl;
	}
}

/// Trace function for the power harvested by the energy harvester.
template <int node>
void HarvestedPower(double oldValue, double newValue)
{
	if (config.filesOutput)
	{
		std::stringstream ss;
		ss << "./ResultColetorSwipt/subdir/harvested_power_" << node << ".csv";
		// ss << "./ResultColetorSwipt/subdir/harvested_power_0.csv";
		static std::fstream f(ss.str().c_str(), std::ios::out);
		f << Simulator::Now().GetSeconds() << ", " << node << ", " << config.psFactor << ", " << newValue - oldValue << ", " << newValue << std::endl;
		// std::cout << std::fixed << std::setprecision(9) << Simulator::Now().GetNanoSeconds() << "ns, harvested power=" << harvestedPower << "W" << std::endl;
		// std::cout << std::fixed << std::setprecision(9) << Simulator::Now().GetSeconds() << "s" << ", Function Call harvested power= "<< newValue << "W" << std::endl;
	}
	config.harvestedPowerCounter[node]++;
	config.g_harvestedPowerAvg[node] = std::abs(newValue);
}

/// Trace function for the total energy harvested by the node.
template <int node>
void TotalEnergyHarvested(double oldValue, double newValue)
{
	if (config.filesOutput)
	{
		std::stringstream ss;
		ss << "./ResultColetorSwipt/subdir/total_energy_harvested_" << node << ".csv";
		// ss << "./ResultColetorSwipt/subdir/total_energy_harvested_0.csv";
		static std::fstream f(ss.str().c_str(), std::ios::out);
		f << Simulator::Now().GetSeconds() << ", " << node << ", " << config.psFactor << ", " << newValue - oldValue << ", " << newValue << std::endl;
		// harvestedEnergy[node] = newValue-oldValue;
		// std::cout << Simulator::Now().GetSeconds() << "s, total energy harvested=" << newValue << "J, nodeId=" << node << std::endl;
	}

	config.harvestedEnergyCounter[node]++;
	config.g_harvestedEnergyAvg[node] = std::abs(newValue);
	//config.g_harvestedEnergyAvg[node] = newValue;
}

double nodePosition(int i)
{
	if ((i>=0) && (i<10))
	{
		return 1.0;
	}
	else if ((i>=10) && (i<20))
	{
		return 1.75;
	}
	else if ((i>=20) && (i<30))
	{
		return 2.5;
	}	
	//else if ((i>=30) && (i<40))
	//{
	//	return 5.0;
	//}	
	//else if ((i>=40) && (i<50))
	//{
	//	return 6.0;
	//}	
	//else if ((i>=50) && (i<config.Nsta))
	//{
	//	return 7.0;
	//}
}

void OnPhyTxDrop2(std::string context, Ptr<const Packet> packet, DropReason reason)
{
   	WifiMacHeader hdr;
    packet->PeekHeader(hdr);

	Mac48Address source = hdr.GetAddr2();
    Mac48Address destination = hdr.GetAddr1();

	//for (int i = 0; i < config.Nsta; i++)
	//{
	//	if (source == macAdd[i])
	//	{
	//		std::stringstream ss;
	//		ss << "./ResultColetorSwipt/subdir/OnPhyTxDrop2.csv";
	//		static std::fstream f(ss.str().c_str(), std::ios::out);
	//		f << Simulator::Now().GetSeconds() << ", " << i << ", " <<  config.psFactor << ", " << dropReason[reason] << std::endl;
	//	}
	//} 
}

void OnPhyRxDrop2(std::string context, Ptr<const Packet> packet, DropReason reason)
{
   	WifiMacHeader hdr;
    packet->PeekHeader(hdr);

	Mac48Address source = hdr.GetAddr2();
    Mac48Address destination = hdr.GetAddr1();

	for (int i = 0; i < config.Nsta; i++)
	{
		if (destination == macAdd[i])
		{
			//std::stringstream ss;
			//ss << "./ResultColetorSwipt/subdir/OnPhyRxDrop2.csv";
			//static std::fstream f(ss.str().c_str(), std::ios::out);
			//f << Simulator::Now().GetSeconds() << ", " << i  << ", " <<  config.psFactor << ", " << dropReason[reason] << std::endl;
		}
	}
}

void OnMacPacketDropped2(std::string context, Ptr<const Packet> packet, DropReason reason) 
{
	//std::stringstream ss;
	//ss << "./ResultColetorSwipt/subdir/OnMacDrop.csv";
	//static std::fstream f(ss.str().c_str(), std::ios::out);
	//f << Simulator::Now().GetSeconds() << ", " << context  <<", " <<  config.psFactor << ", " << dropReason[reason] << std::endl;
}		

template <int node>
void PhyStateTraced(std::string context, Time start, Time duration, enum WifiPhy::State state)
{

	if(config.filesOutput)
	{
    	std::stringstream ss;
		// ss << "/media/jose/ext32v2/network20/state_" << node  << ".csv";
		ss << "./ResultColetorSwipt/subdir/state_" << node << ".csv";
		static std::fstream f(ss.str().c_str(), std::ios::out);
		// f << std::fixed << std::setprecision(9) << Simulator::Now().GetSeconds() << ", " << state << ", " << duration.GetSeconds() << ", " << node << std::endl;
		f << Simulator::Now().GetSeconds() << ", " << node << ", " <<  config.psFactor << ", "  << state << ", " << duration.GetSeconds() <<  std::endl;
		//f << start.GetSeconds () << ", " << node <<", " <<  psFactor << ", "  << state << ", " << duration.GetSeconds() << ", " << node << std::endl;
	
		// std::cout << std::fixed << std::setprecision(9) << Simulator::Now ().GetSeconds () << "s, Node:"<< node << ", state=" << state << ", duration=" << duration << ", context:" << context << std::endl;
	}
}

template <int node>
void RxEndOk (Ptr<const Packet> packet)
{
	WifiMacHeader hdr;
    packet->PeekHeader(hdr);

	Mac48Address source = hdr.GetAddr2();
    Mac48Address destination = hdr.GetAddr1();


	onFramesRxAtNode[node]++;

	//if (destination == apDevice.Get(0)->GetAddress())
	if (node==config.Nsta)
	{
		for (int i = 0; i < config.Nsta; i++)
		{
			//if (source == macAdd[i])
			if (source == Mac48Address(macAdd[i]))
			{
				onFramesRxAtApFromNode[i]++;
				//cout << "Source ADDR at AP: " << source << " , No of Frames " << onFramesRxAtApFromNode[i] << endl;
				if (hdr.IsRetry())
				{
					NumberOfRetriesRx[i]++;
				}
			}
		}
	}

	//for (int i = 0; i < Nsta; i++)
    //{
    //    if (destination == macAdd[i])
    //    {
	//		 onFramesRxAtNode[i]++;
	//	}
	//	
	//	if (destination == apDevice.Get(0)->GetAddress() )
	//	{
	//		if (source == macAdd[i])
	//		{
	//			onFramesRxatAp[i]++;
	//		}
	//	}
	//}
}

template <int node>
void TxEndOk (Ptr<const Packet> packet)
{
	onFramesTx[node]++;
}

template <int node>
void MonitorRx(Ptr<const Packet> packet, uint16_t channelFreqMhz, 
			   uint16_t channelNumber, uint32_t rate, bool isShortPreamble, 
			   WifiTxVector txvector, double signalDbm, double noiseDbm)
{
    WifiMacHeader hdr;
    packet->PeekHeader(hdr);

	Mac48Address source = hdr.GetAddr2();
    Mac48Address destination = hdr.GetAddr1();

	if (config.filesOutput)
	{
		std::stringstream ss;
		ss << "./ResultColetorSwipt/subdir/MonitorRx_" << node <<".csv";
		static std::fstream f(ss.str().c_str(), std::ios::out);
		f << Simulator::Now().GetMicroSeconds() << ", " 
		  //<< dist << ", "
		  << source << ", "	
		  << destination << ", "	
		  //<< node << ", "	
    	  << config.psFactor << ", "
		  << packet->GetSize () << ", "
    	  << channelFreqMhz << ", "
		  << channelNumber << ", "
    	  << rate << ", "
    	  << isShortPreamble << ", "
    	  << txvector << ", "
    	  << signalDbm << ", "
    	  << noiseDbm 
		  << std::endl;
	}


    if (destination==macAdd[node])
    {
        onFramesRxMonitor[node]++;
	    g_signalDbmAvg[node] += ((signalDbm - g_signalDbmAvg[node]) / onFramesRxMonitor[node]);
	    g_noiseDbmAvg[node] += ((noiseDbm - g_noiseDbmAvg[node]) / onFramesRxMonitor[node]);
	    m_rxSignalDbm[node] = signalDbm;

        //if (config.filesOutput)
        //{
	    //    std::stringstream ss;
	    //    ss << "./ResultColetorSwipt/subdir/MonitorRx_" << node <<".csv";
	    //    static std::fstream f(ss.str().c_str(), std::ios::out);
	    //    f << Simulator::Now().GetSeconds() << ", " 
	    //      << dist << ", "
	    //      << node << ", "	
        //      << config.psFactor << ", "
	    //      << packet->GetSize () << ", "
        //      << channelFreqMhz << ", "
	    //      << channelNumber << ", "
        //      << rate << ", "
        //      << isShortPreamble << ", "
        //      << txvector << ", "
        //      << signalDbm << ", "
        //      << noiseDbm 
	    //      << std::endl;
        //}
    }
}		

template <int node>
void MonitorTx (Ptr<const Packet> packet, uint16_t channelFreqMhz,
				uint16_t channelNumber, uint32_t rate, bool isShortPreamble, 
				WifiTxVector txvector)
{
	WifiMacHeader hdr;
    packet->PeekHeader(hdr);

	Mac48Address source = hdr.GetAddr2();
    Mac48Address destination = hdr.GetAddr1();

	std::stringstream ss;
	ss << "./ResultColetorSwipt/subdir/MonitorSniffTx_" << node << ".csv";
	// ss << "/media/jose/extFAT/SWIPT/Simulation2/infra/MonitorSniffTx.csv";
	static std::fstream f(ss.str().c_str(), std::ios::out);
	// f << std::fixed << std::setprecision(9) << Simulator::Now().GetSeconds() << ", " << i <<  ", " << sources.Get (i)->GetRemainingEnergy() << std::endl;
	f << Simulator::Now().GetMicroSeconds() << ", " 
		<< source << ", "	
	  	<< destination << ", "
		<< packet << ", "
	  << channelFreqMhz << ", "
	  << channelNumber << ", "
	  << rate << ", "
	  << isShortPreamble << ", "
	  << txvector << std::endl;
	// cout << txvector << std::endl;
	//onFramesTx[node]++;
}

int main(int argc, char *argv[])
{
	RngSeedManager::SetSeed(config.seed);
	RngSeedManager::SetRun (config.seedRun);

	//LogComponentEnable ("UdpServer", LOG_INFO);
	//LogComponentEnable("UdpEchoServerApplication", LOG_INFO);
	//LogComponentEnable("UdpEchoClientApplication", LOG_INFO);

	//LogComponentEnable ("ApWifiMac", LOG_LEVEL_ALL);
	//LogComponentEnable ("StaWifiMac", LOG_LEVEL_ALL);
	//LogComponentEnable ("EdcaTxopN", LOG_ERROR);
	//LogComponentEnable ("InterferenceHelper", LOG_LEVEL_ALL );
	//LogComponentEnable ("YansErrorRateModel", LOG_LEVEL_ALL );
	//LogComponentEnable ("S1gRawCtr", LOG_LEVEL_ALL );
	//LogComponentEnable ("pageSlice", LOG_LEVEL_ALL );
	//LogComponentEnable ("RPS", LOG_LEVEL_ALL );
	//LogComponentEnable ("TIM", LOG_LEVEL_ALL );
	//LogComponentEnable ("PropagationLossModel", LOG_LEVEL_ALL );

	bool OutputPosition = true;
	config = Configuration(argc, argv);

	config.rps = configureRAW(config.rps, config.RAWConfigFile);
	config.Nsta = config.NRawSta;

	configurePageSlice();
	configureTIM();
	checkRawAndTimConfiguration ();

	config.NSSFile = config.trafficType + "_" + std::to_string(config.Nsta) + "sta_" + std::to_string(ngroup) + "Group_" + std::to_string(nslot) + "slots_" + std::to_string(config.payloadSize) + "payload_" + std::to_string(config.totaltraffic) + "Mbps_" + std::to_string(config.BeaconInterval) + "BI" + ".nss";

	stats = Statistics(config.Nsta);
	eventManager = SimulationEventManager(config.visualizerIP,
										  config.visualizerPort, config.NSSFile);
	
	for (int i=0; i<=config.Nsta; i++)
	{
		config.harvestedPowerCounter[i] = 0;
		config.harvestedEnergyCounter[i] = 0;
		config.g_harvestedPowerAvg[i] = 0.0;
		config.g_harvestedEnergyAvg[i] = 0.0;
		config.NumberOfPacketsToApFromNode[i] = 0;
		config.consump[i] = 0.0;
		config.consumption[i] = 0.0;
		onFramesTx[i] = 0;
		onFramesRxAtNode[i] = 0;
		onFramesRxAtApFromNode[i] = 0;
		onFramesRxMonitor[i] = 0;
		g_signalDbmAvg[i] = 0.0;
		g_noiseDbmAvg[i] = 0.0; 
		m_rxSignalDbm[i] = 0.0;
		NumberOfRetriesRx[i] = 0;
	}
	
	
	uint32_t totalRawGroups(0);
	for (int i = 0; i < config.rps.rpsset.size(); i++)
	{
		uint8_t nRaw = config.rps.rpsset[i]->GetNumberOfRawGroups();
		totalRawGroups += nRaw;
		cout << "Total raw groups after rps " << i << " is " << totalRawGroups << endl;
		for (int j = 0; j < nRaw; j++)
		{
			config.totalRawSlots += config.rps.rpsset[i]->GetRawAssigmentObj(j).GetSlotNum();
			cout << "Total slots after group " << j << " is " << config.totalRawSlots << endl;
		}
	}
	transmissionsPerTIMGroupAndSlotFromAPSinceLastInterval = vector<long>(
		config.totalRawSlots, 0);
	transmissionsPerTIMGroupAndSlotFromSTASinceLastInterval = vector<long>(
		config.totalRawSlots, 0);


	wifiStaNode.Create(config.Nsta);
	wifiApNode.Create(1);


	 YansWifiChannelHelper channelBuilder = YansWifiChannelHelper();

	 channelBuilder.AddPropagationLoss("ns3::LogDistancePropagationLossModel",
			"Exponent", DoubleValue(3.76), 
			"ReferenceLoss", DoubleValue(23.3),
			"ReferenceDistance", DoubleValue(0.99));

	 channelBuilder.SetPropagationDelay(
			"ns3::ConstantSpeedPropagationDelayModel");			


	Ptr<YansWifiChannel> channel = channelBuilder.Create();
	channel->TraceConnectWithoutContext("Transmission",
										MakeCallback(&onChannelTransmission)); // TODO

	YansWifiPhyHelper phy = YansWifiPhyHelper::Default();
	phy.SetErrorRateModel("ns3::YansErrorRateModel");
	phy.SetChannel(channel);
	phy.Set("Frequency", UintegerValue (config.frequency));
	phy.Set("ShortGuardEnabled", BooleanValue(false));
	phy.Set("ChannelWidth", UintegerValue(getBandwidth(config.DataMode))); // changed
	phy.Set("EnergyDetectionThreshold", DoubleValue(-110.0));
	//phy.Set("CcaMode1Threshold", DoubleValue(-113.0));
	phy.Set("CcaMode1Threshold", DoubleValue(-107.0));
	phy.Set("TxGain", DoubleValue(6.0));
	phy.Set("RxGain", DoubleValue(6.0));
	phy.Set("TxPowerLevels", UintegerValue(1));
	phy.Set("TxPowerEnd", DoubleValue(0.0));
	phy.Set("TxPowerStart", DoubleValue(0.0));
	phy.Set("RxNoiseFigure", DoubleValue(6.8));
	phy.Set("LdpcEnabled", BooleanValue(true));
	phy.Set("S1g1MfieldEnabled", BooleanValue(config.S1g1MfieldEnabled));

	WifiHelper wifi = WifiHelper::Default();	
	wifi.SetStandard(WIFI_PHY_STANDARD_80211ah);
	S1gWifiMacHelper mac = S1gWifiMacHelper::Default();

	Ssid ssid = Ssid("ns380211ah");
	StringValue DataRate;
	DataRate = StringValue(getWifiMode(config.DataMode)); // changed

	// disable fragmentation for frames below 22000 bytes
	Config::SetDefault ("ns3::WifiRemoteStationManager::FragmentationThreshold",
					   StringValue("220000"));
	// turn off RTS/CTS for frames below 220000 bytes
	Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold",
					   StringValue("220000"));
	// Fix non-unicast data rate to be the same as that of unicast
	Config::SetDefault ("ns3::WifiRemoteStationManager::NonUnicastMode", DataRate);
	

	wifi.SetRemoteStationManager("ns3::ConstantRateWifiManager", 
								"DataMode", DataRate, 
								"ControlMode", DataRate);
	//wifi.SetRemoteStationManager("ns3::ArfWifiManager");

	mac.SetType("ns3::StaWifiMac",
				"Ssid", SsidValue(ssid),
				"ActiveProbing", BooleanValue(false));
				//"MaxMissedBeacons", UintegerValue(1000));
	//mac.SetBlockAckThresholdForAc (AC_BE, 2);


	NetDeviceContainer staDevice = wifi.Install(phy, mac, wifiStaNode);



	mac.SetType("ns3::ApWifiMac",
				"Ssid", SsidValue(ssid),
				"BeaconInterval", TimeValue(MicroSeconds(config.BeaconInterval)),
				"NRawStations", UintegerValue(config.NRawSta),
				"RPSsetup", RPSVectorValue(config.rps),
				"PageSliceSet", pageSliceValue(config.pageS),
				"TIMSet", TIMValue(config.tim));
				//"NRawGroupStas", UintegerValue(10));
	//mac.SetBlockAckThresholdForAc (AC_BE, 0);


	phy.Set("TxGain", DoubleValue(13.0));
	phy.Set("RxGain", DoubleValue(13.0));
	phy.Set("TxPowerLevels", UintegerValue(1));
	phy.Set("TxPowerEnd", DoubleValue(30.0));
	phy.Set("TxPowerStart", DoubleValue(30.0));
	phy.Set("RxNoiseFigure", DoubleValue(6.8));
	phy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11_RADIO);


	apDevice = wifi.Install(phy, mac, wifiApNode);

	//channelBuilder.AddPropagationLoss("ns3::BuildingsPropagationLossModel",
	//	"ShadowSigmaOutdoor", DoubleValue (10.0));	

	Config::Set(
		"/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/BE_EdcaTxopN/Queue/MaxPacketNumber",
		//"/NodeList/*/DeviceList/0/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/VO_EdcaTxopN/Queue/MaxPacketNumber",
		UintegerValue(10));
	Config::Set(
		"/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/BE_EdcaTxopN/Queue/MaxDelay",
		//"/NodeList/*/DeviceList/0/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/VO_EdcaTxopN/Queue/MaxDelay",
		TimeValue(NanoSeconds(6000000000000)));


	Config::Set("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/Txop/MinCw", UintegerValue(15));
	Config::Set("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/Txop/MaxCw", UintegerValue(1023));
	Config::Set("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/Txop/Aifsn", UintegerValue(3));

	//Config::Set("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/Txop/MinCw", UintegerValue(1));
	//Config::Set("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/Txop/MaxCw", UintegerValue(1));
	//Config::Set("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/Txop/Aifsn", UintegerValue(2));	
	
	/*
		string DataModeCamera = "OfdmRate650KbpsBW2MHz";
		StringValue aa = StringValue(DataModeCamera);
		for (uint16_t k = 0; k < config.Nsta; k++) {
			std::ostringstream APSTA;
			APSTA << k;
			std::string strAP = APSTA.str();
			Config::Set(
					"/NodeList/" + strAP
							+ "/DeviceList/0/$ns3::WifiNetDevice/RemoteStationManager/$ns3::ConstantRateWifiManager/DataMode",
					aa);
			Config::Set(
					"/NodeList/" + strAP
							+ "/DeviceList/0/$ns3::WifiNetDevice/RemoteStationManager/$ns3::ConstantRateWifiManager/ControlMode",
					aa);
			Config::Set(
					"/NodeList/" + strAP
							+ "/DeviceList/0/$ns3::WifiNetDevice/Phy/$ns3::YansWifiPhy/ChannelWidth",
					UintegerValue(2));
		}
	*/
	std::ostringstream oss;
	oss << "/NodeList/" << wifiApNode.Get(0)->GetId()
		<< "/DeviceList/0/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/$ns3::ApWifiMac/";
	Config::ConnectWithoutContext(oss.str() + "RpsIndex", MakeCallback(&RpsIndexTrace));
	Config::ConnectWithoutContext(oss.str() + "RawGroup", MakeCallback(&RawGroupTrace));
	Config::ConnectWithoutContext(oss.str() + "RawSlot", MakeCallback(&RawSlotTrace));
	Config::ConnectWithoutContext(oss.str() + "PacketToApFromNode", MakeCallback(&PacketToApFromNode));

	// mobility.
	MobilityHelper mobility;
	Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator>();

	positionAlloc->Add(Vector(200.0, 200.0, 0.0));

	double angle = 2.0 * M_PI / 10.0; // angle between nodes
	// double angle = 2.0 * M_PI / 5;  // angle between nodes
	Vector center(200.0, 200.0, 0.0); // center of the circle

	Vector position;

	for (int i = 0; i < config.Nsta; ++i)
	{
		// double radius;
		double shift;

		config.dist = nodePosition(i);
		if ((config.dist == 1.0))
		{
			shift = angle / 3;
		}
		else if (config.dist == 1.75)
		{
			//angle = 2.0 * M_PI * config.dist/ 10.0;
			shift = angle * 2 / 3;
		}		
		else if (config.dist == 2.5)
		{
			//angle = 2.0 * M_PI * config.dist/ 10.0;
			shift = angle * 3 / 7;
		}		
		else if (config.dist == 5.0)
		{
			//angle = 2.0 * M_PI * config.dist/ 10.0;
			shift = angle * 4 / 7;
		}		
		else if (config.dist == 6.0)
		{
			angle = 2.0 * M_PI * config.dist/ 10.0;
			shift = angle * 5 / 7;
		}		
		else if (config.dist == 7.0)
		{
			angle = 2.0 * M_PI * config.dist/ 14.0;
			shift = angle * 6 / 7;
		}


		double x = center.x + nodePosition(i) * cos(i * angle + shift);
		double y = center.y + nodePosition(i) * sin(i * angle + shift);

		//double x = center.x + config.dist * cos(i * angle + shift);
		//double y = center.y + config.dist * sin(i * angle + shift);
		double z = center.z;
		Vector position(x, y, z);
		positionAlloc->Add(position);
	}


	mobility.SetPositionAllocator(positionAlloc);
	mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	mobility.Install(wifiApNode);
	mobility.Install(wifiStaNode);

	/*

	 MobilityHelper mobilityApCamera;
	 Ptr<ListPositionAllocator> positionAllocAp = CreateObject<ListPositionAllocator> ();
	 positionAllocAp->Add (Vector (xpos, ypos, 0.0));
	 mobilityApCamera.SetPositionAllocator (positionAllocAp);
	 mobilityApCamera.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
	 mobilityApCamera.Install (wifiApNode);

	 float deltaAngle = 2* M_PI / (config.tcpipcameraEnd - config.tcpipcameraStart +1);
	 float angle = 0.0;
	 double x = 0.0;
	 double y = 0.0;

	 double Distance = 50.0;


	 Ptr<UniformRandomVariable> m_rv = CreateObject<UniformRandomVariable> ();


	 for (int i = config.tcpipcameraStart; i <= config.tcpipcameraEnd; i++)
	 {
	 x = cos(angle) * Distance + xpos;
	 y = sin(angle) * Distance + ypos;

	 MobilityHelper mobilityCamera;
	 Ptr<ListPositionAllocator> positionAllocSta = CreateObject<ListPositionAllocator> ();
	 positionAllocSta->Add(Vector(x, y, 0.0));
	 mobilityCamera.SetPositionAllocator(positionAllocSta);
	 mobilityCamera.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	 mobilityCamera.Install(wifiStaNode.Get(i));
	 angle += deltaAngle;
	 }

	 */

	/* Internet stack*/
	InternetStackHelper stack;
	stack.Install(wifiApNode);
	stack.Install(wifiStaNode);

	Ipv4AddressHelper address;

	address.SetBase("192.168.0.0", "255.255.0.0");

	staNodeInterface = address.Assign(staDevice);
	apNodeInterface = address.Assign(apDevice);

	// trace association
	std::cout << "Configuring trace sources..." << std::endl;
	for (uint16_t kk = 0; kk < config.Nsta; kk++)
	{
		std::ostringstream STA;
		STA << kk;
		std::string strSTA = STA.str();

		assoc_record *m_assocrecord = new assoc_record;
		m_assocrecord->setstaid(kk);
		Config::Connect(
			"/NodeList/" + strSTA + "/DeviceList/0/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/$ns3::StaWifiMac/Assoc",
			MakeCallback(&assoc_record::SetAssoc, m_assocrecord));
		Config::Connect(
			"/NodeList/" + strSTA + "/DeviceList/0/$ns3::WifiNetDevice/Mac/$ns3::RegularWifiMac/$ns3::StaWifiMac/DeAssoc",
			MakeCallback(&assoc_record::UnsetAssoc, m_assocrecord));
		assoc_vector.push_back(m_assocrecord);
	}

	std::cout << "Populating routing tables..." << std::endl;
	Ipv4GlobalRoutingHelper::PopulateRoutingTables();
	std::cout << "Populating ARP cache..." << std::endl;
	PopulateArpCache();

	// configure tracing for associations & other metrics
	std::cout << "Configuring trace sinks for nodes..." << std::endl;
	configureNodes(wifiStaNode, staDevice);

	Config::Connect(
		"/NodeList/" + std::to_string(config.Nsta) + "/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxDropWithReason",
		MakeCallback(&OnAPPhyRxDrop));
	Config::Connect(
		"/NodeList/" + std::to_string(config.Nsta) + "/DeviceList/0/$ns3::WifiNetDevice/Mac/$ns3::ApWifiMac/PacketToTransmitReceivedFromUpperLayer",
		MakeCallback(&OnAPPacketToTransmitReceived));

	Ptr<MobilityModel> mobility1 =
		wifiApNode.Get(0)->GetObject<MobilityModel>();
	Vector apposition = mobility1->GetPosition();
	if (OutputPosition)
	{
		uint32_t i = 0;
		while (i < config.Nsta)
		{
			Ptr<MobilityModel> mobility = wifiStaNode.Get(i)->GetObject<MobilityModel>();
			Vector position = mobility->GetPosition();
			nodes[i]->x = position.x;
			nodes[i]->y = position.y;
			std::cout << "Sta node#" << i << ", "
					  << "position = " << position
					  << std::endl;
			dist[i] = mobility->GetDistanceFrom(
				wifiApNode.Get(0)->GetObject<MobilityModel>());
			i++;
		}
		std::cout << "AP node, position = " << apposition << std::endl;
	}

	for (int i=0; i < config.Nsta; ++i) 
	{
		Ptr<MobilityModel> mobility = wifiStaNode.Get(i)->GetObject<MobilityModel>();
		Vector position = mobility->GetPosition();
		//double x = position.x;
		//double y = position.y;
		//std::cout << "Sta node#" << i << ", " << "position = " << position
		//		<< std::endl;

        //if (config.filesOutput)
        //{
			std::stringstream ss;
			ss << "./ResultColetorSwipt/subdir/Mobility.csv";
			static std::fstream f(ss.str().c_str(), std::ios::out);
			f << i << ", " << nodes[i]->aId << ", " << position.x << ", " << position.y << ", " <<position.z << endl;
        //}
	}

	/*Print of the state of the stations*/
	for (uint32_t i = 0; i < config.Nsta; i++)
	{
		std::ostringstream STA;
		STA << i;
		std::string strSTA = STA.str();

		Config::Connect(
			"/NodeList/" + strSTA + "/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State",
			MakeCallback(&PhyStateTrace));
	}

	eventManager.onStartHeader();
	eventManager.onStart(config);
	if (config.rps.rpsset.size() > 0)
	{
		for (int i = 0; i < config.rps.rpsset.size(); i++)
		{	
			for (int j = 0;
				 j < config.rps.rpsset[i]->GetNumberOfRawGroups(); j++)
				 {
					eventManager.onRawConfig(i, j,
										 config.rps.rpsset[i]->GetRawAssigmentObj(j));
				 }
		}
	}
	for (uint32_t i = 0; i < config.Nsta; i++)
		eventManager.onSTANodeCreated(*nodes[i]);

	eventManager.onAPNodeCreated(apposition.x, apposition.y);
	eventManager.onStatisticsHeader();

	sendStatistics(true);

	/** Energy Model **/
	/***************************************************************************/
	///* energy source */
	LiIonEnergySourceHelper liIonEnergySourceHelper;

	liIonEnergySourceHelper.Set("LiIonEnergySourceInitialEnergyJ", DoubleValue(100));
	liIonEnergySourceHelper.Set("LiIonEnergyLowBatteryThreshold", DoubleValue(0.2));
	liIonEnergySourceHelper.Set("LiIonEnergyHighBatteryThreshold", DoubleValue(0.7));
	liIonEnergySourceHelper.Set("InitialCellVoltage", DoubleValue(1.00));
	liIonEnergySourceHelper.Set("NominalCellVoltage", DoubleValue(1.00));
	liIonEnergySourceHelper.Set("ExpCellVoltage", DoubleValue(1.01));
	liIonEnergySourceHelper.Set("RatedCapacity", DoubleValue(1.00));
	liIonEnergySourceHelper.Set("NomCapacity", DoubleValue(1.01));
	liIonEnergySourceHelper.Set("ExpCapacity", DoubleValue(1.02));
	liIonEnergySourceHelper.Set("InternalResistance", DoubleValue(0.083));
	liIonEnergySourceHelper.Set("TypCurrent", DoubleValue(2.33));
	liIonEnergySourceHelper.Set("ThresholdVoltage", DoubleValue(0.3));
	liIonEnergySourceHelper.Set("PeriodicEnergyUpdateInterval", TimeValue(Seconds(config.simulationTime + config.CoolDownPeriod + 2.0)));
	// install source

				 	cout << "\n\n\n\nRUN UP TILL HERE node: \n\n\n\n" <<endl;
	EnergySourceContainer sources;
	for (int i = 0; i < config.Nsta; i++)
	{
		sources.Add(liIonEnergySourceHelper.Install(wifiStaNode.Get(i)));
	}

	// Power consumption: https://www.asiarf.com/shop/halow-lora-iot/wi-fi-halow-sub-ghz-wireless-module-morse-micro-mm6108-mm610x-001/
	/* device energy model */
	WifiRadioEnergyModelHelper radioEnergyHelper;

	// configure radio energy model
	if (config.datarate < 3900000)
	{
		radioEnergyHelper.Set("TxCurrentA", DoubleValue(8.5e-3));
		radioEnergyHelper.Set("RxCurrentA", DoubleValue(6.0e-3));
	}
	else
	{
		radioEnergyHelper.Set("TxCurrentA", DoubleValue(61.82e-3));
		radioEnergyHelper.Set("RxCurrentA", DoubleValue(27.0e-3));
	}

	radioEnergyHelper.Set("SleepCurrentA", DoubleValue(1.2e-6));
	radioEnergyHelper.Set("IdleCurrentA", DoubleValue(6.0e-3));
	radioEnergyHelper.Set("CcaBusyCurrentA", DoubleValue(6.0e-3));
	radioEnergyHelper.Set("SwitchingCurrentA", DoubleValue(8.5e-3));

	// install device model
	DeviceEnergyModelContainer deviceModels;

	for (int i = 0; i < config.Nsta; i++)
	{
		deviceModels.Add(radioEnergyHelper.Install(staDevice.Get(i), sources.Get(i)));
	}

	/* energy harvester */
	SwiptHarvesterHelper swiptHelper;
	// configure energy harvester
	swiptHelper.Set("AntennaNoise", DoubleValue(-111.0));
	swiptHelper.Set("PowerSplitFactor", DoubleValue (config.psFactor));
	swiptHelper.Set("SwiptEfficiency", DoubleValue(0.9));
	swiptHelper.Set("DCConversionEfficiency", DoubleValue(0.95));
	// install harvester on all energy sources

	EnergyHarvesterContainer harvesters;

	for (int i = 0; i < config.Nsta; i++)
	{
		harvesters.Add(swiptHelper.Install(sources.Get(i)));
	}

	for (uint32_t i = 0; i < config.Nsta; i++)
	{
		Ptr<WifiPhy> phyp = wifiStaNode.Get(i)->GetDevice(0)->GetObject<WifiNetDevice>()->GetPhy();
		Ptr<SwiptHarvester> swiptharvester = harvesters.Get(i)->GetObject<SwiptHarvester>();

		swiptharvester->SetUpSwiptPhyListener(phyp);
		// std::cout << "\n\n=======\n phypp:" << phyp << std::endl;

		Ptr<YansWifiPhy> yansPhy = wifiStaNode.Get(i)->GetDevice(0)->GetObject<WifiNetDevice>()->GetPhy()->GetObject<YansWifiPhy>();

		Ptr<YansWifiChannel> m_channel = wifiStaNode.Get(i)->GetDevice(0)->GetObject<WifiNetDevice>()->GetPhy()->GetChannel()->GetObject<YansWifiChannel>();
		m_channel->AddSwiptPointer(swiptharvester);
		// std::cout << "\n\n=======\n wifiChannelPtr:" << m_channel << "\t" << swiptharvester << std::endl;
	}

		sources.Get(0)->TraceConnectWithoutContext("RemainingEnergy",  MakeCallback(&RemainingEnergy<0>));
		sources.Get(1)->TraceConnectWithoutContext("RemainingEnergy",  MakeCallback(&RemainingEnergy<1>));
		sources.Get(2)->TraceConnectWithoutContext("RemainingEnergy",  MakeCallback(&RemainingEnergy<2>));
		sources.Get(3)->TraceConnectWithoutContext("RemainingEnergy",  MakeCallback(&RemainingEnergy<3>));
		sources.Get(4)->TraceConnectWithoutContext("RemainingEnergy",  MakeCallback(&RemainingEnergy<4>));
		sources.Get(5)->TraceConnectWithoutContext("RemainingEnergy",  MakeCallback(&RemainingEnergy<5>));
		sources.Get(6)->TraceConnectWithoutContext("RemainingEnergy",  MakeCallback(&RemainingEnergy<6>));
		sources.Get(7)->TraceConnectWithoutContext("RemainingEnergy",  MakeCallback(&RemainingEnergy<7>));
		sources.Get(8)->TraceConnectWithoutContext("RemainingEnergy",  MakeCallback(&RemainingEnergy<8>));
		sources.Get(9)->TraceConnectWithoutContext("RemainingEnergy",  MakeCallback(&RemainingEnergy<9>));
		sources.Get(10)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<10>));
		sources.Get(11)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<11>));
		sources.Get(12)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<12>));
		sources.Get(13)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<13>));
		sources.Get(14)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<14>));
		sources.Get(15)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<15>));
		sources.Get(16)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<16>));
		sources.Get(17)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<17>));
		sources.Get(18)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<18>));
		sources.Get(19)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<19>));
		sources.Get(20)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<20>));
		sources.Get(21)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<21>));
		sources.Get(22)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<22>));
		sources.Get(23)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<23>));
		sources.Get(24)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<24>));
		sources.Get(25)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<25>));
		sources.Get(26)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<26>));
		sources.Get(27)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<27>));
		sources.Get(28)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<28>));
		sources.Get(29)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<29>));
		//sources.Get(30)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<30>));
		//sources.Get(31)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<31>));
		//sources.Get(32)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<32>));
		//sources.Get(33)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<33>));
		//sources.Get(34)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<34>));
		//sources.Get(35)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<35>));
		//sources.Get(36)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<36>));
		//sources.Get(37)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<37>));
		//sources.Get(38)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<38>));
		//sources.Get(39)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<39>));
		//sources.Get(40)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<40>));
		//sources.Get(41)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<41>));
		//sources.Get(42)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<42>));
		//sources.Get(43)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<43>));
		//sources.Get(44)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<44>));
		//sources.Get(45)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<45>));
		//sources.Get(46)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<46>));
		//sources.Get(47)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<47>));
		//sources.Get(48)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<48>));
		//sources.Get(49)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<49>));
		//sources.Get(50)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<50>));
		//sources.Get(51)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<51>));
		//sources.Get(52)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<52>));
		//sources.Get(53)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<53>));
		//sources.Get(54)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<54>));
		//sources.Get(55)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<55>));
		//sources.Get(56)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<56>));
		//sources.Get(57)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<57>));
		//sources.Get(58)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<58>));
		//sources.Get(59)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<59>));
		//sources.Get(60)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<60>));
		//sources.Get(61)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<61>));
		//sources.Get(62)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<62>));
		//sources.Get(63)->TraceConnectWithoutContext("RemainingEnergy", MakeCallback(&RemainingEnergy<63>));



		deviceModels.Get(0)->TraceConnectWithoutContext("TotalEnergyConsumption",  MakeCallback(&TotalEnergy<0>));
		deviceModels.Get(1)->TraceConnectWithoutContext("TotalEnergyConsumption",  MakeCallback(&TotalEnergy<1>));
		deviceModels.Get(2)->TraceConnectWithoutContext("TotalEnergyConsumption",  MakeCallback(&TotalEnergy<2>));
		deviceModels.Get(3)->TraceConnectWithoutContext("TotalEnergyConsumption",  MakeCallback(&TotalEnergy<3>));
		deviceModels.Get(4)->TraceConnectWithoutContext("TotalEnergyConsumption",  MakeCallback(&TotalEnergy<4>));
		deviceModels.Get(5)->TraceConnectWithoutContext("TotalEnergyConsumption",  MakeCallback(&TotalEnergy<5>));
		deviceModels.Get(6)->TraceConnectWithoutContext("TotalEnergyConsumption",  MakeCallback(&TotalEnergy<6>));
		deviceModels.Get(7)->TraceConnectWithoutContext("TotalEnergyConsumption",  MakeCallback(&TotalEnergy<7>));
		deviceModels.Get(8)->TraceConnectWithoutContext("TotalEnergyConsumption",  MakeCallback(&TotalEnergy<8>));
		deviceModels.Get(9)->TraceConnectWithoutContext("TotalEnergyConsumption",  MakeCallback(&TotalEnergy<9>));
		deviceModels.Get(10)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<10>));
		deviceModels.Get(11)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<11>));
		deviceModels.Get(12)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<12>));
		deviceModels.Get(13)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<13>));
		deviceModels.Get(14)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<14>));
		deviceModels.Get(15)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<15>));
		deviceModels.Get(16)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<16>));
		deviceModels.Get(17)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<17>));
		deviceModels.Get(18)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<18>));
		deviceModels.Get(19)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<19>));
		deviceModels.Get(20)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<20>));
		deviceModels.Get(21)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<21>));
		deviceModels.Get(22)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<22>));
		deviceModels.Get(23)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<23>));
		deviceModels.Get(24)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<24>));
		deviceModels.Get(25)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<25>));
		deviceModels.Get(26)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<26>));
		deviceModels.Get(27)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<27>));
		deviceModels.Get(28)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<28>));
		deviceModels.Get(29)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<29>));
		//deviceModels.Get(30)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<30>));
		//deviceModels.Get(31)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<31>));
		//deviceModels.Get(32)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<32>));
		//deviceModels.Get(33)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<33>));
		//deviceModels.Get(34)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<34>));
		//deviceModels.Get(35)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<35>));
		//deviceModels.Get(36)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<36>));
		//deviceModels.Get(37)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<37>));
		//deviceModels.Get(38)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<38>));
		//deviceModels.Get(39)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<39>));
		//deviceModels.Get(40)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<40>));
		//deviceModels.Get(41)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<41>));
		//deviceModels.Get(42)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<42>));
		//deviceModels.Get(43)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<43>));
		//deviceModels.Get(44)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<44>));
		//deviceModels.Get(45)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<45>));
		//deviceModels.Get(46)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<46>));
		//deviceModels.Get(47)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<47>));
		//deviceModels.Get(48)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<48>));
		//deviceModels.Get(49)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<49>));
		//deviceModels.Get(50)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<50>));
		//deviceModels.Get(51)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<51>));
		//deviceModels.Get(52)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<52>));
		//deviceModels.Get(53)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<53>));
		//deviceModels.Get(54)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<54>));
		//deviceModels.Get(55)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<55>));
		//deviceModels.Get(56)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<56>));
		//deviceModels.Get(57)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<57>));
		//deviceModels.Get(58)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<58>));
		//deviceModels.Get(59)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<59>));
		//deviceModels.Get(60)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<60>));
		//deviceModels.Get(61)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<61>));
		//deviceModels.Get(62)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<62>));
		//deviceModels.Get(63)->TraceConnectWithoutContext("TotalEnergyConsumption", MakeCallback(&TotalEnergy<63>));

		harvesters.Get(0)->TraceConnectWithoutContext("HarvestedPower",  MakeCallback(&HarvestedPower<0>));
		harvesters.Get(1)->TraceConnectWithoutContext("HarvestedPower",  MakeCallback(&HarvestedPower<1>));
		harvesters.Get(2)->TraceConnectWithoutContext("HarvestedPower",  MakeCallback(&HarvestedPower<2>));
		harvesters.Get(3)->TraceConnectWithoutContext("HarvestedPower",  MakeCallback(&HarvestedPower<3>));
		harvesters.Get(4)->TraceConnectWithoutContext("HarvestedPower",  MakeCallback(&HarvestedPower<4>));
		harvesters.Get(5)->TraceConnectWithoutContext("HarvestedPower",  MakeCallback(&HarvestedPower<5>));
		harvesters.Get(6)->TraceConnectWithoutContext("HarvestedPower",  MakeCallback(&HarvestedPower<6>));
		harvesters.Get(7)->TraceConnectWithoutContext("HarvestedPower",  MakeCallback(&HarvestedPower<7>));
		harvesters.Get(8)->TraceConnectWithoutContext("HarvestedPower",  MakeCallback(&HarvestedPower<8>));
		harvesters.Get(9)->TraceConnectWithoutContext("HarvestedPower",  MakeCallback(&HarvestedPower<9>));
		harvesters.Get(10)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<10>));
		harvesters.Get(11)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<11>));
		harvesters.Get(12)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<12>));
		harvesters.Get(13)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<13>));
		harvesters.Get(14)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<14>));
		harvesters.Get(15)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<15>));
		harvesters.Get(16)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<16>));
		harvesters.Get(17)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<17>));
		harvesters.Get(18)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<18>));
		harvesters.Get(19)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<19>));
		harvesters.Get(20)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<20>));
		harvesters.Get(21)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<21>));
		harvesters.Get(22)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<22>));
		harvesters.Get(23)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<23>));
		harvesters.Get(24)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<24>));
		harvesters.Get(25)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<25>));
		harvesters.Get(26)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<26>));
		harvesters.Get(27)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<27>));
		harvesters.Get(28)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<28>));
		harvesters.Get(29)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<29>));
		//harvesters.Get(30)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<30>));
		//harvesters.Get(31)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<31>));
		//harvesters.Get(32)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<32>));
		//harvesters.Get(33)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<33>));
		//harvesters.Get(34)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<34>));
		//harvesters.Get(35)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<35>));
		//harvesters.Get(36)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<36>));
		//harvesters.Get(37)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<37>));
		//harvesters.Get(38)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<38>));
		//harvesters.Get(39)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<39>));
		//harvesters.Get(40)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<40>));
		//harvesters.Get(41)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<41>));
		//harvesters.Get(42)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<42>));
		//harvesters.Get(43)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<43>));
		//harvesters.Get(44)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<44>));
		//harvesters.Get(45)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<45>));
		//harvesters.Get(46)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<46>));
		//harvesters.Get(47)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<47>));
		//harvesters.Get(48)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<48>));
		//harvesters.Get(49)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<49>));
		//harvesters.Get(20)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<50>));
		//harvesters.Get(51)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<51>));
		//harvesters.Get(52)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<52>));
		//harvesters.Get(53)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<53>));
		//harvesters.Get(54)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<54>));
		//harvesters.Get(55)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<55>));
		//harvesters.Get(56)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<56>));
		//harvesters.Get(57)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<57>));
		//harvesters.Get(58)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<58>));
		//harvesters.Get(59)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<59>));
		//harvesters.Get(60)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<60>));
		//harvesters.Get(61)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<61>));
		//harvesters.Get(62)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<62>));
		//harvesters.Get(63)->TraceConnectWithoutContext("HarvestedPower", MakeCallback(&HarvestedPower<63>));

		harvesters.Get(0)->TraceConnectWithoutContext("TotalEnergyHarvested",  MakeCallback(&TotalEnergyHarvested<0>));
		harvesters.Get(1)->TraceConnectWithoutContext("TotalEnergyHarvested",  MakeCallback(&TotalEnergyHarvested<1>));
		harvesters.Get(2)->TraceConnectWithoutContext("TotalEnergyHarvested",  MakeCallback(&TotalEnergyHarvested<2>));
		harvesters.Get(3)->TraceConnectWithoutContext("TotalEnergyHarvested",  MakeCallback(&TotalEnergyHarvested<3>));
		harvesters.Get(4)->TraceConnectWithoutContext("TotalEnergyHarvested",  MakeCallback(&TotalEnergyHarvested<4>));
		harvesters.Get(5)->TraceConnectWithoutContext("TotalEnergyHarvested",  MakeCallback(&TotalEnergyHarvested<5>));
		harvesters.Get(6)->TraceConnectWithoutContext("TotalEnergyHarvested",  MakeCallback(&TotalEnergyHarvested<6>));
		harvesters.Get(7)->TraceConnectWithoutContext("TotalEnergyHarvested",  MakeCallback(&TotalEnergyHarvested<7>));
		harvesters.Get(8)->TraceConnectWithoutContext("TotalEnergyHarvested",  MakeCallback(&TotalEnergyHarvested<8>));
		harvesters.Get(9)->TraceConnectWithoutContext("TotalEnergyHarvested",  MakeCallback(&TotalEnergyHarvested<9>));
		harvesters.Get(10)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<10>));
		harvesters.Get(11)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<11>));
		harvesters.Get(12)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<12>));
		harvesters.Get(13)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<13>));
		harvesters.Get(14)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<14>));
		harvesters.Get(15)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<15>));
		harvesters.Get(16)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<16>));
		harvesters.Get(17)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<17>));
		harvesters.Get(18)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<18>));
		harvesters.Get(19)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<19>));
		harvesters.Get(20)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<20>));
		harvesters.Get(21)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<21>));
		harvesters.Get(22)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<22>));
		harvesters.Get(23)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<23>));
		harvesters.Get(24)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<24>));
		harvesters.Get(25)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<25>));
		harvesters.Get(26)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<26>));
		harvesters.Get(27)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<27>));
		harvesters.Get(28)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<28>));
		harvesters.Get(29)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<29>));
		//harvesters.Get(30)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<30>));
		//harvesters.Get(31)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<31>));
		//harvesters.Get(32)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<32>));
		//harvesters.Get(33)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<33>));
		//harvesters.Get(34)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<34>));
		//harvesters.Get(35)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<35>));
		//harvesters.Get(36)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<36>));
		//harvesters.Get(37)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<37>));
		//harvesters.Get(38)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<38>));
		//harvesters.Get(39)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<39>));
		//harvesters.Get(40)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<40>));
		//harvesters.Get(41)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<41>));
		//harvesters.Get(42)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<42>));
		//harvesters.Get(43)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<43>));
		//harvesters.Get(44)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<44>));
		//harvesters.Get(45)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<45>));
		//harvesters.Get(46)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<46>));
		//harvesters.Get(47)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<47>));
		//harvesters.Get(48)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<48>));
		//harvesters.Get(49)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<49>));
		//harvesters.Get(50)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<50>));
		//harvesters.Get(51)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<51>));
		//harvesters.Get(52)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<52>));
		//harvesters.Get(53)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<53>));
		//harvesters.Get(54)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<54>));
		//harvesters.Get(55)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<55>));
		//harvesters.Get(56)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<56>));
		//harvesters.Get(57)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<57>));
		//harvesters.Get(58)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<58>));
		//harvesters.Get(59)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<59>));
		//harvesters.Get(60)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<60>));
		//harvesters.Get(61)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<61>));
		//harvesters.Get(62)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<62>));
		//harvesters.Get(63)->TraceConnectWithoutContext("TotalEnergyHarvested", MakeCallback(&TotalEnergyHarvested<63>));

		//Config::Connect("/NodeList/0/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State",  MakeCallback(&PhyStateTraced<0>));
		//Config::Connect("/NodeList/1/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State",  MakeCallback(&PhyStateTraced<1>));
		Config::Connect("/NodeList/2/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State",  MakeCallback(&PhyStateTraced<2>));
		//Config::Connect("/NodeList/3/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State",  MakeCallback(&PhyStateTraced<3>));
		//Config::Connect("/NodeList/4/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State",  MakeCallback(&PhyStateTraced<4>));
		//Config::Connect("/NodeList/5/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State",  MakeCallback(&PhyStateTraced<5>));
		//Config::Connect("/NodeList/6/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State",  MakeCallback(&PhyStateTraced<6>));
		//Config::Connect("/NodeList/7/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State",  MakeCallback(&PhyStateTraced<7>));
		//Config::Connect("/NodeList/8/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State",  MakeCallback(&PhyStateTraced<8>));
		//Config::Connect("/NodeList/9/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State",  MakeCallback(&PhyStateTraced<9>));
		//Config::Connect("/NodeList/10/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State", MakeCallback(&PhyStateTraced<10>));
		//Config::Connect("/NodeList/11/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State", MakeCallback(&PhyStateTraced<11>));
		Config::Connect("/NodeList/12/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State", MakeCallback(&PhyStateTraced<12>));
		//Config::Connect("/NodeList/13/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State", MakeCallback(&PhyStateTraced<13>));
		//Config::Connect("/NodeList/14/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State", MakeCallback(&PhyStateTraced<14>));
		//Config::Connect("/NodeList/15/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State", MakeCallback(&PhyStateTraced<15>));
		//Config::Connect("/NodeList/16/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State", MakeCallback(&PhyStateTraced<16>));
		//Config::Connect("/NodeList/17/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State", MakeCallback(&PhyStateTraced<17>));
		//Config::Connect("/NodeList/18/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State", MakeCallback(&PhyStateTraced<18>));
		//Config::Connect("/NodeList/19/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State", MakeCallback(&PhyStateTraced<19>));
		//Config::Connect("/NodeList/20/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State", MakeCallback(&PhyStateTraced<20>));
		//Config::Connect("/NodeList/21/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State", MakeCallback(&PhyStateTraced<21>));
		Config::Connect("/NodeList/22/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State", MakeCallback(&PhyStateTraced<22>));
		//Config::Connect("/NodeList/23/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State", MakeCallback(&PhyStateTraced<23>));
		//Config::Connect("/NodeList/24/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State", MakeCallback(&PhyStateTraced<24>));
		//Config::Connect("/NodeList/25/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State", MakeCallback(&PhyStateTraced<25>));
		//Config::Connect("/NodeList/26/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State", MakeCallback(&PhyStateTraced<26>));
		//Config::Connect("/NodeList/27/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State", MakeCallback(&PhyStateTraced<27>));
		//Config::Connect("/NodeList/28/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State", MakeCallback(&PhyStateTraced<28>));
		//Config::Connect("/NodeList/29/DeviceList/*/Phy/$ns3::YansWifiPhy/State/State", MakeCallback(&PhyStateTraced<29>));

        Config::ConnectWithoutContext("/NodeList/0/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd",  MakeCallback(&TxEndOk<0>));
		Config::ConnectWithoutContext("/NodeList/1/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd",  MakeCallback(&TxEndOk<1>));
		Config::ConnectWithoutContext("/NodeList/2/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd",  MakeCallback(&TxEndOk<2>));
		Config::ConnectWithoutContext("/NodeList/3/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd",  MakeCallback(&TxEndOk<3>));
		Config::ConnectWithoutContext("/NodeList/4/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd",  MakeCallback(&TxEndOk<4>));
		Config::ConnectWithoutContext("/NodeList/5/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd",  MakeCallback(&TxEndOk<5>));
		Config::ConnectWithoutContext("/NodeList/6/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd",  MakeCallback(&TxEndOk<6>));
		Config::ConnectWithoutContext("/NodeList/7/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd",  MakeCallback(&TxEndOk<7>));
		Config::ConnectWithoutContext("/NodeList/8/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd",  MakeCallback(&TxEndOk<8>));
		Config::ConnectWithoutContext("/NodeList/9/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd",  MakeCallback(&TxEndOk<9>));
		Config::ConnectWithoutContext("/NodeList/10/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd", MakeCallback(&TxEndOk<10>));
		Config::ConnectWithoutContext("/NodeList/11/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd", MakeCallback(&TxEndOk<11>));
		Config::ConnectWithoutContext("/NodeList/12/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd", MakeCallback(&TxEndOk<12>));
		Config::ConnectWithoutContext("/NodeList/13/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd", MakeCallback(&TxEndOk<13>));
		Config::ConnectWithoutContext("/NodeList/14/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd", MakeCallback(&TxEndOk<14>));
		Config::ConnectWithoutContext("/NodeList/15/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd", MakeCallback(&TxEndOk<15>));
        Config::ConnectWithoutContext("/NodeList/16/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd", MakeCallback(&TxEndOk<16>));
		Config::ConnectWithoutContext("/NodeList/17/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd", MakeCallback(&TxEndOk<17>));
		Config::ConnectWithoutContext("/NodeList/18/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd", MakeCallback(&TxEndOk<18>));
		Config::ConnectWithoutContext("/NodeList/19/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd", MakeCallback(&TxEndOk<19>));
		Config::ConnectWithoutContext("/NodeList/20/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd", MakeCallback(&TxEndOk<20>));
		Config::ConnectWithoutContext("/NodeList/21/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd", MakeCallback(&TxEndOk<21>));
		Config::ConnectWithoutContext("/NodeList/22/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd", MakeCallback(&TxEndOk<22>));
		Config::ConnectWithoutContext("/NodeList/23/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd", MakeCallback(&TxEndOk<23>));
		Config::ConnectWithoutContext("/NodeList/24/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd", MakeCallback(&TxEndOk<24>));
		Config::ConnectWithoutContext("/NodeList/25/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd", MakeCallback(&TxEndOk<25>));
		Config::ConnectWithoutContext("/NodeList/26/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd", MakeCallback(&TxEndOk<26>));
		Config::ConnectWithoutContext("/NodeList/27/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd", MakeCallback(&TxEndOk<27>));
		Config::ConnectWithoutContext("/NodeList/28/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd", MakeCallback(&TxEndOk<28>));
		Config::ConnectWithoutContext("/NodeList/29/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd", MakeCallback(&TxEndOk<29>));
		//Config::ConnectWithoutContext("/NodeList/30/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyTxEnd", MakeCallback(&TxEndOk<30>));

		Config::ConnectWithoutContext("/NodeList/0/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd",  MakeCallback(&RxEndOk<0>));
		Config::ConnectWithoutContext("/NodeList/1/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd",  MakeCallback(&RxEndOk<1>));
		Config::ConnectWithoutContext("/NodeList/2/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd",  MakeCallback(&RxEndOk<2>));
		Config::ConnectWithoutContext("/NodeList/3/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd",  MakeCallback(&RxEndOk<3>));
		Config::ConnectWithoutContext("/NodeList/4/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd",  MakeCallback(&RxEndOk<4>));
		Config::ConnectWithoutContext("/NodeList/5/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd",  MakeCallback(&RxEndOk<5>));
		Config::ConnectWithoutContext("/NodeList/6/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd",  MakeCallback(&RxEndOk<6>));
		Config::ConnectWithoutContext("/NodeList/7/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd",  MakeCallback(&RxEndOk<7>));
		Config::ConnectWithoutContext("/NodeList/8/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd",  MakeCallback(&RxEndOk<8>));
		Config::ConnectWithoutContext("/NodeList/9/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd",  MakeCallback(&RxEndOk<9>));
		Config::ConnectWithoutContext("/NodeList/10/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd", MakeCallback(&RxEndOk<10>));
		Config::ConnectWithoutContext("/NodeList/11/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd", MakeCallback(&RxEndOk<11>));
		Config::ConnectWithoutContext("/NodeList/12/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd", MakeCallback(&RxEndOk<12>));
		Config::ConnectWithoutContext("/NodeList/13/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd", MakeCallback(&RxEndOk<13>));
		Config::ConnectWithoutContext("/NodeList/14/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd", MakeCallback(&RxEndOk<14>));
		Config::ConnectWithoutContext("/NodeList/15/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd", MakeCallback(&RxEndOk<15>));
		Config::ConnectWithoutContext("/NodeList/16/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd", MakeCallback(&RxEndOk<16>));
		Config::ConnectWithoutContext("/NodeList/17/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd", MakeCallback(&RxEndOk<17>));
		Config::ConnectWithoutContext("/NodeList/18/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd", MakeCallback(&RxEndOk<18>));
		Config::ConnectWithoutContext("/NodeList/19/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd", MakeCallback(&RxEndOk<19>));
		Config::ConnectWithoutContext("/NodeList/20/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd", MakeCallback(&RxEndOk<20>));
		Config::ConnectWithoutContext("/NodeList/21/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd", MakeCallback(&RxEndOk<21>));
		Config::ConnectWithoutContext("/NodeList/22/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd", MakeCallback(&RxEndOk<22>));
		Config::ConnectWithoutContext("/NodeList/23/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd", MakeCallback(&RxEndOk<23>));
		Config::ConnectWithoutContext("/NodeList/24/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd", MakeCallback(&RxEndOk<24>));
		Config::ConnectWithoutContext("/NodeList/25/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd", MakeCallback(&RxEndOk<25>));
		Config::ConnectWithoutContext("/NodeList/26/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd", MakeCallback(&RxEndOk<26>));
		Config::ConnectWithoutContext("/NodeList/27/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd", MakeCallback(&RxEndOk<27>));
		Config::ConnectWithoutContext("/NodeList/28/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd", MakeCallback(&RxEndOk<28>));
		Config::ConnectWithoutContext("/NodeList/29/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd", MakeCallback(&RxEndOk<29>));
		Config::ConnectWithoutContext("/NodeList/30/DeviceList/0/$ns3::WifiNetDevice/Phy/PhyRxEnd", MakeCallback(&RxEndOk<30>));
                
		Config::ConnectWithoutContext("/NodeList/0/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",   MakeCallback(&MonitorRx<0>));
        Config::ConnectWithoutContext("/NodeList/1/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",   MakeCallback(&MonitorRx<1>));
        Config::ConnectWithoutContext("/NodeList/2/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",   MakeCallback(&MonitorRx<2>));
        Config::ConnectWithoutContext("/NodeList/3/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",   MakeCallback(&MonitorRx<3>));
        Config::ConnectWithoutContext("/NodeList/4/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",   MakeCallback(&MonitorRx<4>));
        Config::ConnectWithoutContext("/NodeList/5/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",   MakeCallback(&MonitorRx<5>));
        Config::ConnectWithoutContext("/NodeList/6/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",   MakeCallback(&MonitorRx<6>));
        Config::ConnectWithoutContext("/NodeList/7/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",   MakeCallback(&MonitorRx<7>));
        Config::ConnectWithoutContext("/NodeList/8/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",   MakeCallback(&MonitorRx<8>));
        Config::ConnectWithoutContext("/NodeList/9/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",   MakeCallback(&MonitorRx<9>));
        Config::ConnectWithoutContext("/NodeList/10/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",  MakeCallback(&MonitorRx<10>));
        Config::ConnectWithoutContext("/NodeList/11/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",  MakeCallback(&MonitorRx<11>));
        Config::ConnectWithoutContext("/NodeList/12/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",  MakeCallback(&MonitorRx<12>));
        Config::ConnectWithoutContext("/NodeList/13/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",  MakeCallback(&MonitorRx<13>));
        Config::ConnectWithoutContext("/NodeList/14/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",  MakeCallback(&MonitorRx<14>));
        Config::ConnectWithoutContext("/NodeList/15/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",  MakeCallback(&MonitorRx<15>));
        Config::ConnectWithoutContext("/NodeList/16/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",  MakeCallback(&MonitorRx<16>));
        Config::ConnectWithoutContext("/NodeList/17/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",  MakeCallback(&MonitorRx<17>));
        Config::ConnectWithoutContext("/NodeList/18/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",  MakeCallback(&MonitorRx<18>));
        Config::ConnectWithoutContext("/NodeList/19/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",  MakeCallback(&MonitorRx<19>));
        Config::ConnectWithoutContext("/NodeList/20/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",  MakeCallback(&MonitorRx<20>));
        Config::ConnectWithoutContext("/NodeList/21/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",  MakeCallback(&MonitorRx<21>));
        Config::ConnectWithoutContext("/NodeList/22/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",  MakeCallback(&MonitorRx<22>));
        Config::ConnectWithoutContext("/NodeList/23/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",  MakeCallback(&MonitorRx<23>));
        Config::ConnectWithoutContext("/NodeList/24/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",  MakeCallback(&MonitorRx<24>));
        Config::ConnectWithoutContext("/NodeList/25/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",  MakeCallback(&MonitorRx<25>));
        Config::ConnectWithoutContext("/NodeList/26/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",  MakeCallback(&MonitorRx<26>));
        Config::ConnectWithoutContext("/NodeList/27/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",  MakeCallback(&MonitorRx<27>));
        Config::ConnectWithoutContext("/NodeList/28/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",  MakeCallback(&MonitorRx<28>));
        Config::ConnectWithoutContext("/NodeList/29/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",  MakeCallback(&MonitorRx<29>));
        Config::ConnectWithoutContext("/NodeList/30/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferRx",  MakeCallback(&MonitorRx<30>));

	/*

		Config::ConnectWithoutContext("/NodeList/0/DeviceList/0/$ns3::WifiNetDevice/Phy/MonitorSnifferTx",  MakeCallback(&MonitorTx<30>));
	*/

	if(config.filesOutput)
	{
		phy.EnablePcap("./ResultColetorSwipt/subdir/apTrace", apDevice.Get(0), true); // Output what we are doing
		phy.EnablePcap("./ResultColetorSwipt/subdir/staDevice", staDevice.Get(2)); // Output what we are doing
		phy.EnablePcap("./ResultColetorSwipt/subdir/staDevice", staDevice.Get(12)); // Output what we are doing
		phy.EnablePcap("./ResultColetorSwipt/subdir/staDevice", staDevice.Get(22)); // Output what we are doing
	}

	// Flow monitor
	//Ptr<FlowMonitor> flowMonitor;
	//FlowMonitorHelper flowHelper;
	//flowMonitor = flowHelper.InstallAll();

  	//AthstatsHelper athstats;
  	//athstats.EnableAthstats("./ResultColetorSwipt/subdir/basic-athstats-node" , wifiStaNode);


	Simulator::Stop(Seconds(config.simulationTime + config.CoolDownPeriod)); // allow up to a minute after the client & server apps are finished to process the queue
	Simulator::Run();

	//flowMonitor->SerializeToXmlFile("./ResultColetorSwipt/subdir/test3.xml", true, true);



	// Visualizer throughput
	int pay = 0, totalSuccessfulPackets = 0, totalSentPackets = 0, totalPacketsEchoed = 0;
	for (int i = 0; i < config.Nsta; i++)
	{
		totalSuccessfulPackets += stats.get(i).NumberOfSuccessfulPackets;
		totalSentPackets += stats.get(i).NumberOfSentPackets;
		totalPacketsEchoed += stats.get(i).NumberOfSuccessfulRoundtripPackets;
		pay += stats.get(i).TotalPacketPayloadSize;
		cout << i << " sent: " << stats.get(i).NumberOfSentPackets
			 << " ; delivered: " << stats.get(i).NumberOfSuccessfulPackets
			 << " ; echoed: " << stats.get(i).NumberOfSuccessfulRoundtripPackets
			 << "; packetloss: "
			 << stats.get(i).GetPacketLoss(config.trafficType) << endl;
	}

	double throughput = 0;
	//uint32_t totalPacketsThrough = 0;
	if (config.trafficType == "udp")
	{
		throughput = totalSuccessfulPackets * config.payloadSize * 8./(config.simulationTime + config.CoolDownPeriod - 3);
		uint32_t totalPacketsThrough =
				DynamicCast<UdpServer>(serverApp.Get(0))->GetReceived();
		cout << "totalPacketsThrough " << totalPacketsThrough << " ++my "
			 << totalSuccessfulPackets << endl;
		cout << "throughput " << throughput << " ++my "
			 << pay * 8. / (config.simulationTime + config.CoolDownPeriod - 3) << endl;
		std::cout << "datarate"
				  << "\t"
				  << "throughput" << std::endl;
		std::cout << config.datarate << "bps\t\t" << throughput << "bps" << std::endl;
	}
	else if (config.trafficType == "udpecho")
	{
		double ulThroughput = 0, dlThroughput = 0;
		ulThroughput = totalSuccessfulPackets * config.payloadSize * 8 / (config.simulationTime * 1000000.0);
		dlThroughput = totalPacketsEchoed * config.payloadSize * 8 / (config.simulationTime * 1000000.0);
		cout << "totalPacketsSent " << totalSentPackets << endl;
		cout << "totalPacketsDelivered " << totalSuccessfulPackets << endl;
		cout << "totalPacketsEchoed " << totalPacketsEchoed << endl;
		cout << "UL packets lost " << totalSentPackets - totalSuccessfulPackets << endl;
		cout << "DL packets lost " << totalSuccessfulPackets - totalPacketsEchoed << endl;
		cout << "Total packets lost " << totalSentPackets - totalPacketsEchoed << endl;

		/*cout << "uplink throughput Mbit/s " << ulThroughput << endl;
		cout << "downlink throughput Mbit/s " << dlThroughput << endl;*/

		double throughput = (totalSuccessfulPackets + totalPacketsEchoed) * config.payloadSize * 8 / (config.simulationTime * 1000000.0);
		cout << "total throughput Kbit/s " << throughput * 1000 << endl;

		std::cout << "datarate"
				  << "\t"
				  << "throughput" << std::endl;
		std::cout << config.datarate << "\t" << throughput * 1000 << " Kbit/s" << std::endl;
	}
	cout << "total packet loss % "
		 << 100 - 100. * totalSuccessfulPackets / totalSentPackets << endl;

	//for (int i = 0; i < config.rps.rpsset.size(); i++)
	//{
	//	int nRaw = config.rps.rpsset[i]->GetNumberOfRawGroups();
	//	totalRawGroups += nRaw;
	//	cout << "Total raw groups after rps " << i << " is " << totalRawGroups << endl;
	//	for (int j = 0; j < nRaw; j++)
	//	{
	//		config.totalRawSlots += config.rps.rpsset[i]->GetRawAssigmentObj(j).GetSlotNum();
	//		cout << "Total slots after group " << j << " is " << config.totalRawSlots << endl;
	//	}
	//}
	//if (config.rps.rpsset.size() > 0)
	//{
	//for (uint32_t k = 0 ; k < config.rps.getlen(); k++)
	//{
	//for (int i = 0; i < config.rps.rpsset.size(); i++)
	//for (int i = 0; i < config.rps.rpsset.size(); i++)
	//{
	//	for (int j = 0; j < config.rps.rpsset[i]->GetNumberOfRawGroups(); j++)
	//	{
			//std::stringstream ss;
			//ss << "./ResultColetorSwipt/subdir/RpsInfo.csv";
			//static std::fstream f(ss.str().c_str(), std::ios::out);
			//f 	<< i << ", "
			//	<< j << ", "
			//	<< totalRawGroups << ", "
			//	<< config.totalRawSlots << ", "
			//	<< (int)config.rps.rpsset[i]->GetNumberOfRawGroups() << ", "
			//	<< config.rps.getlen() << ", "
			//	<< transmissionsPerTIMGroupAndSlotFromAPSinceLastInterval[i] << ", "
			//	<< transmissionsPerTIMGroupAndSlotFromSTASinceLastInterval[i] << ", "
			//	<< (int)config.rps.rpsset[i]->GetInformationFieldSize() << ", "
			//	<< (int)config.rps.rpsset[i]->GetRawAssigmentObj(j).GetRawGroup() << ", "
			//	<< (int)config.rps.rpsset[i]->GetRawAssigmentObj(j).GetRawSlot() << ", "
			//	<< (int)config.rps.rpsset[i]->GetRawAssigmentObj(j).GetSlotNum() << ", "
			//	<< (int)config.rps.rpsset[i]->GetRawAssigmentObj(j).GetRawGroupAIDStart() << ", "
			//	<< (int)config.rps.rpsset[i]->GetRawAssigmentObj(j).GetRawGroupAIDEnd() 
			//	<< std::endl;
	//	}
	//}
	//}

		for (int i = 0; i < config.Nsta; i++)
        {

			config.consumption[i] = timeRxArray[i].GetSeconds() * 6.0e-3
								  + timeIdleArray[i].GetSeconds() * 6.0e-3
								  + timeCollisionArray[i].GetSeconds() * 6.0e-3
								  + timeTxArray[i].GetSeconds() * 8.5e-3
								  + timeSleepArray[i].GetSeconds() * 1.2e-6;

			config.consump[i] 	  = config.consumption[i]
								  + stats.TimeWhenEverySTAIsAssociated.GetSeconds() * 6.0e-3;			



		    std::stringstream ss;
		    //f << "/media/jose/ext32v2/network20/Summary_" << i << ".csv";
		    ss << "./ResultColetorSwipt/subdir/Summary.csv";
		    static std::fstream f(ss.str().c_str(), std::ios::out);
		    f << i << ", "
			<< nodes[i]->aId << ", "
			<< nodes[i]->isAssociated << ", "	
			<< (int)nodes[i]->rawGroupNumber << ", "
			<< (int)nodes[i]->rawSlotIndex << ", "
			<< nodes[i]->rpsIndex << ", "
		    << std::fixed << std::setprecision(0)
            << config.datarate << ", "

		    << std::fixed << std::setprecision(2)
		    << nodePosition (i) << ", "
			
			//Throughput:
		    << std::fixed << std::setprecision(2)

			<< throughput << ", "
		    << (stats.get(i).NumberOfSuccessfulPackets + stats.get(i).NumberOfSuccessfulRoundtripPackets) * config.payloadSize * 8.0/ ((config.simulationTime + config.CoolDownPeriod - 3.0) * 1.0) << ", " //throughput

			//UDP:
		    << std::fixed << std::setprecision(0)
		    << stats.get(i).NumberOfSentPackets << ", " 				
		    << stats.get(i).NumberOfSuccessfulPackets << ", "			
		    << stats.get(i).TotalPacketSentReceiveTime << ", "	//% Loss
		    << stats.get(i).latency << ", "	//% Loss
		    << stats.get(i).jitter << ", "	//% Loss
		    << stats.get(i).jitterAcc << ", "	//% Loss
		    << stats.get(i).getNumberOfDroppedPackets() << ", "	//% Loss
		    << stats.get(i).GetPacketLoss(config.trafficType) << ", "	//% Loss

			//Frames TX:
			//<< stats.get(i).NumberOfSuccessfullTx << ", " 
			<< onFramesTx[i]  << ", " 
			//Frames RX @ AP:
			//<< config.NumberOfPacketsToApFromNode[i] << ", "
			<< onFramesRxAtApFromNode[i] << ", "
			//Frames RX @ node:
			<< onFramesRxAtNode[i] << ", "
			//Frame Loss %
		    << 100 - 100. * onFramesRxAtApFromNode[i] / onFramesTx[i] << ", " //
			<< stats.get(i).NumberOfTransmissionsDropped << ", " 
			<< stats.get(i).NumberOfReceivesDropped << ", " 
			<< stats.get(i).NumberOfReceiveDroppedByDestination << ", " 

			//psFactor
    	    << std::setprecision(10) << std::scientific
		    << config.psFactor << ", "

		    << config.g_harvestedPowerAvg[i] / config.harvestedPowerCounter[i] << ", "
		    << config.harvestedPowerCounter[i] << ", "
		    << config.g_harvestedPowerAvg[i] << ", "
		    << config.g_harvestedEnergyAvg[i] / config.harvestedEnergyCounter[i] << ", "
		    << config.harvestedEnergyCounter[i] << ", "
		    << config.g_harvestedEnergyAvg[i] << ", "
		    << config.consumption[i] << ", "
		    << config.consump[i] << ", "
			<< stats.get(i).NumberOfCollisions << ", " 
			<< stats.get(i).TotalNumberOfBackedOffSlots << ", " 
			<< stats.get(i).NumberOfMACTxMissedACK << ", "
			<< stats.get(i).NumberOfMACTxMissedACKAndDroppedPacket << ", "
			<< stats.get(i).NumberOfRetriesTx << ", "
			<< stats.get(i).NumberOfRetriesDroppedTx << ", "
			<< NumberOfRetriesRx[i] << ", "
			<< stats.get(i).NumberOfRetriesDroppedRx << ", "
			<< stats.get(i).NumberOfMACTxRTSFailed << ", "
			<< stats.get(i).NumberOfTransmissionsCancelledDueToCrossingRAWBoundary << ", "
			<< stats.get(i).NumberOfBeaconsMissed << ", "

			<< std::fixed << std::setprecision(2)
			<< stats.TimeWhenEverySTAIsAssociated << ", "

		    << std::fixed << std::setprecision(2)
		    << g_signalDbmAvg[i] << ", "
		    << g_noiseDbmAvg[i] << ", "
		    << (g_signalDbmAvg[i] - g_noiseDbmAvg[i])
		    //<< std::log10(snrRxOk[i])*10 << ", "
		    //<< std::log10(snrRxError[i])*10 << ", " 
		    ////<< std::fixed << std::setprecision(6)
		    ////<< std::fixed << std::setprecision(6)
		    //<< (apTxPower - g_signalDbmAvg[i])
		    //<< std::fixed << std::setprecision(2)
    	    //<< csr[i]  << ", "
    	    //<< std::setprecision(10) << std::scientific
    	    //<< ((std::pow (10.0, g_signalDbmAvg[i]/10.0)*1.0e-3)/psFactor)
            << std::endl;
        }
	 


	Simulator::Destroy();

	ofstream risultati;
	string addressresults = config.OutputPath + "moreinfo.txt";
	risultati.open(addressresults.c_str(), ios::out | ios::trunc);

	risultati << "Sta node#,distance,timerx(notassociated),timeidle(notassociated),timetx(notassociated),timesleep(notassociated),timecollision(notassociated)" << std::endl;
	//risultati << "Sta node#,distance,timerx,timeidle,timetx,timesleep,timecollision" << std::endl;
	int i = 0;
	string spazio = ",";

	while (i < config.Nsta)
	{

		risultati << i << spazio << dist[i] << spazio << timeRxArray[i].GetSeconds() << ",(" << timeRxNotAssociated[i].GetSeconds() << ")," << timeIdleArray[i].GetSeconds() << ",(" << timeIdleNotAssociated[i].GetSeconds() << ")," << timeTxArray[i].GetSeconds() << ",(" << timeTxNotAssociated[i].GetSeconds() << ")," << timeSleepArray[i].GetSeconds() << ",(" << timeSleepNotAssociated[i].GetSeconds() << ")," << timeCollisionArray[i].GetSeconds() << ",(" << timeCollisionNotAssociated[i].GetSeconds() << ")" << std::endl;

		cout << "================== Sleep time: " << stats.get(i).TotalSleepTime.GetSeconds() << endl;
		cout << "================== Tx time: " << stats.get(i).TotalTxTime.GetSeconds() << endl;
		cout << "================== Rx time: " << stats.get(i).TotalRxTime.GetSeconds() << endl;
		cout << "+++++++++++++++++++IDLE time: " << stats.get(i).TotalIdleTime.GetSeconds() << endl;
		//cout << "ooooooooooooooooooo TOTENERGY " << stats.get(i).TotalSleepTime.GetSeconds()
		//										+	stats.get(i).TotalTxTime.GetSeconds()
		//										+	stats.get(i).TotalRxTime.GetSeconds()
		//										+	stats.get(i).TotalIdleTime.GetSeconds()
		//										 << " J" << endl;
		cout << "ooooooooooooooooooo TOTENERGY " << stats.get(i).GetTotalEnergyConsumption() << " J" << endl;
		//cout << "Rx+Idle ENERGY " << stats.get(i).EnergyRxIdle << " mW" << endl;
		//cout << "Tx ENERGY " << stats.get(i).EnergyTx << " mW" << endl;

		i++;
	}

	risultati.close();
	return 0;
}
